class OpenWeatherMapAPI {

	async get_weather(chartjs_format = false) {
		let forecast_data = await this.get_forecast();
		let history_data = await this.get_history();
		let data = this.format(history_data, forecast_data);
		if (chartjs_format) {
			data = this.format_chartjs(data);
		}
		return data;
	} // end get_weather()

	format(history_data, forecast_data) {
		let data = {
			current_status: forecast_data.current.weather[0].id,
			timeline: []
		};
		// data for before now
		history_data.hourly.forEach( (hour_data) => {
			data.timeline.push({
				datetime: this.format_time(hour_data.dt),
				temperature: hour_data.temp
			});
		});
		// data for now
		data.timeline.push({
			datetime: this.format_time(forecast_data.current.dt),
			temperature: forecast_data.current.temp
		});
		// date for after now (forecast)
		forecast_data.hourly.forEach( (hour_data) => {
			data.timeline.push({
				datetime: this.format_time(hour_data.dt),
				temperature: hour_data.temp
			});
		});

		return data;
	} // end format()

	format_chartjs(data) {
		let chartjs_data = {
			current_status: data.current_status,
			datetime: [],
			temperature_history: [],
			temperature_forecast: []
		};

		data.timeline.forEach( (datapoint) => {
			let date = moment(datapoint.datetime, "DD.MM.YYYY HH:mm");
			if (moment().isSame(date, 'day')) {
				chartjs_data.datetime.push(moment(date).format("HH"));
				let in_past = date.isSameOrBefore(moment());
				chartjs_data.temperature_history.push(in_past ? datapoint.temperature : null);
				chartjs_data.temperature_forecast.push(in_past ? null : datapoint.temperature);
			}
		});

		let last_empty_index = chartjs_data.temperature_forecast.lastIndexOf(null);
		let first_empty_index = chartjs_data.temperature_history.indexOf(null);
		chartjs_data.temperature_forecast[last_empty_index] = chartjs_data.temperature_history[first_empty_index - 1];

		return chartjs_data;
	} // end format_chartjs()

	format_time(api_timestamp, format = "DD.MM.YYYY HH:mm") {
		return moment(api_timestamp * 1000).format(format);
	} // end format_time()

	async get_forecast() {
		let forecast_data = await this.do_request("https://api.openweathermap.org/data/2.5/onecall?lat=49.873159&lon=8.641948&lang=de&units=metric&appid=0564703fc5df2dc8485bd8093daf8a4e");
		return forecast_data;
	} // end get_forecast()

	async get_history() {
		let timestamp = Math.floor(Date.now() / 1000) - 60;
		let history_data = await this.do_request("https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=49.873159&lon=8.641948&dt=" + timestamp + "&lang=de&units=metric&appid=0564703fc5df2dc8485bd8093daf8a4e");
		return history_data;
	} // end get_history()

	do_request(url) {
		return $.ajax({
			url: url
		});
	} // end do_request()

} // end class
