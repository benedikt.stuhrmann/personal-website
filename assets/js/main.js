/**
 * Stores the identifier, as well as top- and bottom-position for each section of the page.
 * @type {Array}
 */
let sections = null;
/**
 * Decides if the mesh-animation should be played or not.
 * @type {boolean}
 */
let animate;

/**
 * Options for the mesh-animation.
 * @type {{defaultRadius: number, linkRadius: number, defaultSpeed: number, variantSpeed: number, variantRadius: number, particleColor: string, lineColor: string, particleAmount: number}}
 */
let opts = {
  particleColor: "rgb(247,248,243)",
  lineColor: "rgb(247,248,243)",
  particleAmount: 100,
  defaultSpeed: 0.05,
  variantSpeed: 0.2,
  defaultRadius: 2,
  variantRadius: 2,
  linkRadius: 350,
};

$(document).ready( () => {

  prepareAnim();

  // scrolls/jumps to the section specified in the url on pageload
  updateSectionInfo();
  let section_index = sections.findIndex(section => section.identifier === anchor);
  window.scrollTo(0, sections[section_index].top);

  // show nav
  setTimeout(function() {
    $("header").removeClass("out");
    setTimeout(function() {
      $("header").css("transition", "none");
    }, 800);
  }, 300);

  if (typeof typingAnimation === "function") {
    typingAnimation($("#welcome h1"));
  }

  // smooth scroll to destination when using nav-arrows
  $(".scroll-nav").on("click", function(e) {
    let target = $(this).data("target");
    $("html, body").animate({
      scrollTop: $("#" + target).offset().top
    }, 800, "swing", () => {
      //moveHeader(target);
    });
  });

  let secret_timer;
  $("#logo").hover(function() {
      if (!secret_timer) {
        secret_timer = window.setTimeout(function() {
          secret_timer = null;
          $(".badge-inner").removeClass("locked");
        }, 3000);
      }
    },
    function () {
      if (secret_timer) {
        window.clearTimeout(secret_timer);
        secret_timer = null;
      }
      else {
        // add locked again?
      }
    }
  ); // end logo hover

  $(window).scroll($.debounce( 10, function() {
    updateHeader();
  }));

}); // end ready()

/**
 * Gets called when the typing animation finished.
 */
function typingAnimationDone() {
  $("#welcome .hr").removeClass("out");
  $("#me-short").removeClass("out");
  setTimeout(function () {
    $("#home .page-down").removeClass("out");
  }, 1000);
} // end typingAnimationDone()

/**
 * Updates the sections array.
 */
function updateSectionInfo() {
  sections = [];
  $("section").each( (index, section) => {
    let top = Math.floor($(section).offset().top);
    sections.push({
      identifier: $(section).attr("id"),
      top: top,
      bottom: Math.floor(top + $(section).height())
    });
  });
} // end updateSectionInfo()

/**
 * Checks if the scroll position is inside the specified section.
 * @param section     Section object containing top- and bottom-position of the section.
 * @param scroll_pos  The current scroll position (middle of the viewport).
 * @returns {boolean} True if the scroll position is inside the section. False otherwise.
 */
function inSection(section, scroll_pos) {
  return (scroll_pos > section.top && scroll_pos < section.bottom);
} // end inSection()

/**
 * Updates header color and active menu element depending on current scroll.
 * Changes URL according to current section.
 */
function updateHeader() {
  let scroll = $(document).scrollTop();

  sections.forEach( (section) => {
    if (inSection(section, scroll + 40)) {
      $("header").removeClass().addClass(section.identifier);
      $("nav li").removeClass("active");
      $("nav a[data-target='" + section.identifier + "']").closest("li").addClass("active");


      let url = section.identifier;

      if (lang === "de") {
        let url_mappings = {
          home:     "home",
          projects: "projekte",
          contact:  "kontakt"
        };
        url = url_mappings[section.identifier];
      }

      window.history.replaceState(null, "Benedikt Stuhrmann", url);
    }
  });

} // end updateHeader()

function prepareAnim() {
  setupMeshAnimOptions();
  devicePerformanceTest();
} // end prepareAnim()

/**
 * Determines mesh animation options by screen size.
 */
function setupMeshAnimOptions() {
  let canvas = document.getElementById("mesh-anim");
  let canvas_size = canvas.clientHeight * canvas.clientWidth; // in "square-pixels"

  opts.particleAmount = Math.ceil(27.08281 + 0.00005 * canvas_size + 4.071739e-11*canvas_size^2);
  opts.linkRadius = Math.ceil(-7.936507936507937e-11*canvas_size^3  +  0.00035 * canvas_size  +  142.85714285714286);

  if (device === "mobile") {
    opts.defaultRadius = 0.8;
    opts.variantRadius = 1;
  };
} // end setupMeshAnimOptions()

/**
 * Tests the devices performance to decide whether the mesh-animation should be played or not.
 */
function devicePerformanceTest() {
  let ctx = document.getElementById('canvas').getContext('2d');
  let img = document.getElementById('img');

  let draw = function(load) {
    let angle = 0.01;
    ctx.clearRect(0,0,400,400);
    ctx.save();
    ctx.translate(200,200);
    for (let i=0; i<load; i++) {
      ctx.rotate(angle);
      ctx.drawImage(img, 0, 0);
    }
    ctx.restore();
  };

  let t, previousTime;
  let drawLoad = 10;
  let slowCount = 0;
  let maxSlow = 10;
  // Note, you might need to polyfill performance.now and requestAnimationFrame
  t = previousTime = performance.now();
  let tick = function() {
    let maximumFrameTime = 1000/30; // 30 FPS
    t = performance.now();
    let elapsed = t - previousTime;
    previousTime = t;
    if (drawLoad <= 800 && (elapsed < maximumFrameTime || slowCount < maxSlow)) {
      if (elapsed < maximumFrameTime) {
        drawLoad+=10;
      } else {
        slowCount++;
      }
      draw(drawLoad);
      requestAnimationFrame(tick);
    } else {
      // found maximum sustainable load at 30 FPS
      /*document.write("could draw "+(drawLoad)+" in " +
        maximumFrameTime + " ms");*/
      animate = (drawLoad > 500);
      /*console.log("Animate: " + animate);*/

      resizeReset();
      setup();
    }
  };
  requestAnimationFrame(tick);
} // end devicePerformanceTest()
