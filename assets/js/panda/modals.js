if (!show_info_modal) {
window.MODAL_CLOSED = "modal_closed";
window.MODAL_CONFIRMED = "modal_confirmed";

document.addEventListener("DOMContentLoaded", function() {

	// close modal
	Array.from(document.querySelectorAll(".modal .close-modal")).forEach( (close_elem) => {
		close_elem.addEventListener("click", function(e) {
			this.closest(".modal-container").classList.add("hidden");
			this.closest(".modal").dispatchEvent(new CustomEvent(MODAL_CLOSED));
		});
	});

	// confirm modal
	Array.from(document.querySelectorAll(".modal .confirm-modal")).forEach( (confirm_elem) => {
		confirm_elem.addEventListener("click", function(e) {
			this.closest(".modal").dispatchEvent(new CustomEvent(MODAL_CONFIRMED));
		});
	});

});

/**
 * Shows the info-modal, displaying a simple text.
 * @param options
 */
function show_info_modal(options) {
	let modal_elem = document.querySelector("#info-modal");

	let default_options = {
		title: "Simple info modal",
		text: "Info modal content text.",
		content: null,
		btn_close_text: "Close",
		btn_close_class: ""
	};
	options = {...default_options, ...options};
	options.btn_close_class = options.btn_close_class ? options.btn_close_class.split(",") : null;

	modal_elem.querySelector(".modal-title").innerHTML = options.title;
	if (options.content) {
		modal_elem.querySelector(".modal-body").innerHTML = options.content;
	} else if (options.text) {
		modal_elem.querySelector(".modal-body .info-text").innerHTML = options.text;
	}

	let close_btn = modal_elem.querySelector(".modal-footer .btn.close-modal");
	close_btn.innerHTML = options.btn_close_text;
	if (options.btn_close_class) {
		close_btn.classList.add(...options.btn_close_class);
	}

	modal_elem.closest(".modal-container").classList.remove("hidden");

	modal_elem.removeEventListener(MODAL_CLOSED);
	return modal_elem;
} // end show_info_modal()

/**
 * Hides the info-modal.
 */
function close_info_modal() {
	document.querySelector("#info-modal").closest(".modal-container").classList.add("hidden");
} // end close_info_modal()

/**
 * Shows the confirm-modal, displaying content and allowing to confirm or decline/abort something.
 * @param options
 */
function show_confirm_modal(options) {
	let modal_elem = document.querySelector("#confirm-modal");

	let default_options = {
		title: "Simple confirm modal",
		text: "Info confirm content text.",
		content: null,
		btn_close_text: "Close",
		btn_close_class: "",
		btn_confirm_text: "Confirm",
		btn_confirm_class: "btn-primary"
	};
	options = {...default_options, ...options};
	options.btn_close_class = options.btn_close_class ? options.btn_close_class.split(",") : null;
	options.btn_confirm_class = options.btn_confirm_class ? options.btn_confirm_class.split(",") : null;

	modal_elem.querySelector(".modal-title").innerHTML = options.title;
	if (options.content) {
		modal_elem.querySelector(".modal-body").innerHTML = options.content;
	} else if (options.text) {
		modal_elem.querySelector(".modal-body .info-text").innerHTML = options.text;
	}

	let close_btn = modal_elem.querySelector(".modal-footer .btn.close-modal");
	close_btn.innerHTML = options.btn_close_text;
	if (options.btn_close_class) {
		close_btn.classList.add(...options.btn_close_class);
	}
	let confirm_btn = modal_elem.querySelector(".modal-footer .btn.confirm-modal");
	confirm_btn.innerHTML = options.btn_confirm_text;
	if (options.btn_confirm_class) {
		confirm_btn.classList.add(...options.btn_confirm_class);
	}

	modal_elem.closest(".modal-container").classList.remove("hidden");

	return modal_elem;
} // end show_confirm_modal()

/**
 * Hides the confirm-modal.
 */
function close_confirm_modal() {
	document.querySelector("#confirm-modal").closest(".modal-container").classList.add("hidden");
} // end close_info_modal()

/**
 * Disables the confirm modal.
 */
function disable_confirm_modal() {
	toggle_disabled("#confirm-modal .btn.confirm-modal", true)
} // end disable_confirm_modal()

/**
 * Re-enables the confirm modal.
 */
function enable_confirm_modal() {
	toggle_disabled("#confirm-modal .btn.confirm-modal", false)
} // end enable_confirm_modal()

}