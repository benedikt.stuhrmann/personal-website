NodeList.prototype.for = function(callback) {
	let nodes = Array.from(this);
	for (let i = 0; i < nodes.length; i++) {
		callback(nodes[i], i);
	}
};

document.addEventListener("DOMContentLoaded", function() {

	// logout
	Array.from(document.querySelectorAll(".logout")).forEach( function(element) {
		element.addEventListener("click", function(e) {
			logout_handler(e);
		});
	});

	// parsley
	if (window.Parsley) {
		window.Parsley.on("field:error", function(e) {
			// change color of all icons in this form group
			bulk_change_classes(this.$element[0], ".form-group", ".icon", ["parsley-error"], "add");
		});

		window.Parsley.on("field:success", function() {
			// change color of all icons in this form group
			bulk_change_classes(this.$element[0], ".form-group", ".icon", ["parsley-success"], "add");
		});
	}
}, false); // end DOMContentLoaded

/**
 * Called when logout button got clicked. Handles logout process.
 * @param e Submit event.
 */
function logout_handler(e) {
	$.when(try_logout()).done(function (result) {
		result = JSON.parse(result);
		if (result === true) {
			window.location.href = BASE_URL + "panda/login";
		} else if (result === false) {
			console.log("Unable to log out");
		} else {
			console.log("Error while trying to log out. Try again later.");
		}
	});
} // end logout_handler()

/**
 * Tries to logout the user.
 * @returns {*|jQuery|{getAllResponseHeaders, abort, setRequestHeader, readyState, getResponseHeader, overrideMimeType, statusCode}}
 */
function try_logout() {
	return $.ajax({
		url: BASE_URL + "panda/login/logout",
		type: "POST"
	});
} // end try_logout()

/**
 * Adds or removes classes to all icons inside a specified element.
 * (All item_selectors inside the closest_selector from the origin_elem).
 * @param origin_elem									An element. The root of the selection.
 * @param {string} closest_selector		Selector string. Going to select the closest_selector from the origin_elem.
 * @param {string} item_selector			Find all item_selector elements in the selection.
 * @param {array} class_names					An array of classnames to add or remove.
 * @param {string} action							Either "add" (default) or "remove".
 */
function bulk_change_classes(origin_elem, closest_selector, item_selector, class_names, action = "add") {
	let items = origin_elem.closest(closest_selector).querySelectorAll(item_selector);
	Array.from(items).forEach( function(item) {
		class_names.forEach( function(class_name) {
			if (action === "add") {
				item.classList.add(class_name);
			} else if (action === "remove") {
				item.classList.remove(class_name);
			}
		});
	});
} // end bulk_change_classes()

/**
 * Enables or disables elements.
 * @param selector	CSS selector to retrieve element(s) to disable/enable.
 * @param disabled	Whether element(s) should be enabled or disabled.
 */
function toggle_disabled(selector, disabled = true) {
	let elements = document.querySelectorAll(selector);
	elements.for((element, index) => {
		if (disabled) {
			element.classList.add("disabled");
			element.setAttribute("disabled", "disabled");
		} else {
			element.classList.remove("disabled");
			element.removeAttribute("disabled", "disabled");
		}
	});
} // end toggle_disabled()

function disabled(node, e) {
	if (node.classList.contains("disabled") || (node.closest(".btn") && node.closest(".btn").classList.contains("disabled"))) {
		e.preventDefault();
		return true;
	} else {
		return false
	}
} // end check_disabled()

/**
 * Creates a DOM node from a html string.
 * @param html_string The html string.
 * @returns {(() => (Node | null)) | ActiveX.IXMLDOMNode | ChildNode}
 */
function createNodeFromString(html_string) {
	let div = document.createElement("div");
	div.innerHTML = html_string.trim();
	return div.firstChild;
} // end createNodeFromString()

Node.prototype.insertAfter = function(newNode, referenceNode) {
	referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
};
