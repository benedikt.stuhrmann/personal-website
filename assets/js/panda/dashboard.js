let performance = 0;

const widgets = {
	1: {
		chart: null,
		create: function () {
			return "" +
				"<div class='weather-widget'>" +
					"<div class='info'>" +
						"<div class='current'>" +
							"<div class='icon sunny'>" + $("#weather-icon-sunny").html() + "</div>"  +
							"<div class='icon clouds hidden'>" + $("#weather-icon-clouds").html() + "</div>"  +
							"<div class='icon drizzle hidden'>" + $("#weather-icon-drizzle").html() + "</div>"  +
							"<div class='icon rain hidden'>" + $("#weather-icon-rain").html() + "</div>"  +
							"<div class='icon thunder hidden'>" + $("#weather-icon-thunder").html() + "</div>"  +
							"<div class='icon snow hidden'>" + $("#weather-icon-snow").html() + "</div>"  +
							"<span class='temperature'>23°C</span>"  +
						"</div>" +
						"<span class='location'>Wetter Darmstadt</span>"  +
					"</div>" +
					"<div class='chart'>" +
						"<canvas></canvas>" +
					"</div>" +
				"</div>";
		},
		update: async function () {
			console.log("Update weather widget");
			if (!this.chart) {
				let ctx = document.querySelector(".weather-widget canvas").getContext("2d");
				// icons: https://thenounproject.com/term/day-drizzle/2597/
				this.chart = new Chart(ctx, {
					type: "line",
					options: {
						responsive: true,
						maintainAspectRatio: false,
						tooltips: false,
						plugins: {
							legend: {
								display: false
							},
							tooltip: {
								enabled: false
							}
						},
						scales: {
							x: {
								display: false,
								grid: {
									display: false,
									drawBorder: false
								}
							},
							y: {
								display: false,
								grid: {
									display: false,
									drawBorder: false
								}
							}
						}
					},
					data: {
						datasets: [
							{
								data: [],
								borderColor: "orange",
								borderWidth: 5,
								cubicInterpolationMode: "monotone",
								tension: 0.4,
								pointRadius: 0,
								backgroundColor: "orange"
							}, {
								data: [],
								borderDash: [10,5],
								borderColor: "rgba(255, 165, 0, 0.6)",
								borderWidth: 3,
								cubicInterpolationMode: "monotone",
								tension: 0.4,
								pointRadius: 0
							}
						]
					}
				});
			}

			let api = new OpenWeatherMapAPI()
			let data = await api.get_weather(true);
			console.log(data);

			this.chart.data.labels = data.datetime;
			this.chart.data.datasets[0].data = data.temperature_history;
			this.chart.data.datasets[1].data = data.temperature_forecast;
			let radius = new Array(data.temperature_history.length).fill(0);
			radius[data.temperature_history.indexOf(null) - 1] = 5;
			this.chart.data.datasets[0].pointRadius = radius;

			this.chart.update();

			$(".weather-widget .icon").addClass("hidden");
			let icon = this.get_icon(data.current_status);
			$(".weather-widget .icon." + icon).removeClass("hidden");
			let current_temperature = data.temperature_history[data.temperature_history.indexOf(null) - 1];
			$(".weather-widget .temperature").html(Math.round(current_temperature) + "°C");
		},
		auto_update: function (minutes = 5) {
			this.update();
			setTimeout(function () {
				this.auto_update(minutes);
			}.bind(this), 1000 * 60 * minutes);
		},
		get_icon: function (status) {
			// https://docplayer.org/docview/51/27706028/#file=/storage/51/27706028/27706028.pdf
			switch (status) {
				case 200:
				case 201:
				case 202:
				case 210:
				case 211:
				case 212:
				case 221:
				case 230:
				case 231:
				case 232:
					return "thunder";
				case 300:
					return "light-drizzle";
				case 301:
				case 302:
				case 310:
				case 311:
				case 312:
				case 313:
				case 314:
				case 321:
					return "drizzle";
				case 500:
				case 501:
				case 502:
				case 511:
				case 520:
				case 521:
				case 522:
				case 531:
					return "rain";
				case 503:
				case 504:
					return "heavy-rain"
				case 600:
				case 615:
					return "light-snow";
				case 601:
				case 602:
				case 611:
				case 612:
				case 616:
				case 620:
				case 621:
				case 622:
					return "snow";
				case 701:
				case 721:
					return "light-fog";
				case 711:
				case 731:
				case 741:
				case 751:
				case 761:
				case 762:
				case 771:
				case 781:
					return "fog";
				case 800:
				case 801:
				case 802:
				case 803:
				case 804:
					return "sunny";
				case 906:
					return "hail";
				default:
					return "";
			}
		}
	},
	2: {
		create: function () {
			return "" +
				"<div class='performance-widget'>" +
					"<div>" +
						"<div>" +
							$("#performance-arrow-icon").html() +
						"</div>" +
					"</div>" +
					"<div>" +
						"<div>" +
							"<span>0,00%</span><br>" +
							"<span>Portfolio Performance</span>" +
						"</div>" +
					"</div>" +
				"</div>";
		},
		update: function () {
			console.log("Update performance widget");
			// update text
			performance = 0;
			$(".performance-widget span:first-of-type").text(performance.toFixed(2).replace(".", ",") + "%");
			// update styles
			let $svg = $(".performance-widget svg");
			$svg.removeClass("positive negative");
			if (performance > 0) {
				$svg.addClass("positive");
			} else if (performance < 0) {
				$svg.addClass("negative");
			}
		},
		auto_update: function (minutes = 10) {
			this.update();
			setTimeout(function () {
				this.auto_update(minutes);
			}.bind(this), 1000 * 60 * minutes);
		},
	}
};


document.addEventListener("DOMContentLoaded", function() {

	let grid = GridStack.init();
	grid.load([
		{ width: 3, height: 2, content: "1" },
		{ width: 2, height: 1, content: "2" }
	]);
	grid.disable();

	const edit_layout_btn = document.querySelector("[data-action='edit-layout']");
	const save_layout_btn = document.querySelector("[data-action='save-layout']");
	const delete_widget_btn = document.querySelector("[data-action='delete-widget']");

	edit_layout_btn.addEventListener("click", function (e) {
		edit_layout_btn.classList.add("hidden");
		save_layout_btn.classList.remove("hidden");
		grid.enable();
	});

	save_layout_btn.addEventListener("click", function (e) {
		delete_widget_btn.classList.add("hidden");
		save_layout_btn.classList.add("hidden");
		edit_layout_btn.classList.remove("hidden");
		grid.disable();
		let items = grid.save()
		console.log(items);
	});

	grid.on("dragstart", function(e, elem) {
		delete_widget_btn.classList.remove("hidden");
	});

	grid.on("dragstop", function(e, elem) {
		delete_widget_btn.classList.add("hidden");
	});


	$(".grid-stack-item-content").each( (index, widget_container) => {
		let widget = widgets[$(widget_container).text()];
		$(widget_container).html(widget.create());
		if (widget.auto_update) {
			widget.auto_update();
		}
	});

}, false); // end DOMContentLoaded
