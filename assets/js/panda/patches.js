let bottom = false;
let scrolled_over_bottom = 0;
let loading_more = false;

let current_number_of_patches = 0;
let total_number_of_patches = 0;

let event_source;
let patch_count = 0;
let patches_executed = 0;
let current_patch;

let error_data;

document.addEventListener("DOMContentLoaded", function() {

	// get new patches
	update_patches_list("unexecuted");
	// get old patches for archive
	update_patches_list("executed");

	progressbar.init("#patch-execution-progress");

	document.querySelector("#execute-patches").addEventListener("click", function (e) {
		if (disabled(this, e)) {
			return;
		}
		event_source = new EventSource(BASE_URL + "panda/patches/execute?lang=" + LANG);
		event_source.addEventListener("exec_patch_open", handle_exec_patch_open);
		event_source.addEventListener("exec_patch_begin", handle_exec_patch_begin);
		event_source.addEventListener("exec_patch_exception", handle_exec_patch_exception);
		event_source.addEventListener("exec_patch_end", handle_exec_patch_end);
		event_source.addEventListener("exec_patch_close", handle_exec_patch_close);
	});

	function patches_executed() {
		console.log("DONE");
	} // end patches_executed()

	// listener for clicking the load-more-patches button
	document.querySelector(".btn.load-more-results").addEventListener("click", async function(e) {
		update_patches_list("executed");
	});

	// show load more patches button, when the user scrolled down to the bottom of the page
	window.onscroll = function(e) {
		let current_scroll = document.documentElement.scrollTop;
		let position = window.innerHeight + current_scroll;

		// check if scrolled  bottom of the page
		if (position >= document.body.scrollHeight) {
			bottom = true;
			if (current_number_of_patches < total_number_of_patches) {
				// if so and there are more patches, show the load more button
				document.querySelector(".btn.load-more-results").classList.add("show");
			}
		} else {
			bottom = false;
		}
	};

	// trigger loading more patches when scrolling over bottom of page
	document.addEventListener("wheel", function(e) {
		if (bottom && e.deltaY > 0 && !loading_more) {
			// if user scrolled to the bottom and is trying to scroll further and patches are not already loading => fill threshold
			scrolled_over_bottom += e.deltaY;
		} else {
			// otherwise reset it
			scrolled_over_bottom = 0;
		}

		if (scrolled_over_bottom > 0) {
			// if user scrolled over bottom => fill button indicating when loading is going to happen
			let perc = Math.min(Math.max(scrolled_over_bottom / 10, 0), 100);
			document.querySelector(".scroll-controlled").style.height = perc + "%";

			if (perc === 100 && !loading_more) {
				// when threshold is filled => load new patches
				scrolled_over_bottom = 0;
				update_patches_list("executed");
			}
		} else {
			document.querySelector(".scroll-controlled").style.height = "0";
		}
	});

}, false); // end DOMContentLoaded

/**
 * Starts an ajax request to get available database patches.
 * @param type 					Type of db-patches. Either "executed" or "unexecuted" (default).
 * @param offset 				Offset in the result-set. Defaults to 0.
 * @param newest_first	Order patches, so newest ones are first (default).
 *                      When set to false, oldest ones will be first.
 * @param search				Search string for filtering results.
 * @returns {Promise<unknown>} A promise with the loaded patches, the number of loaded patches and the total number of patches.
 */
function get_patches(type = "executed", offset = 0, newest_first = true, search = null) {
	return new Promise( (resolve, reject) => {
		$.ajax({
			url: BASE_URL + "panda/patches/getPatches?lang=" + LANG,
			type: "POST",
			data: {
				type: type,
				offset: offset,
				newest_first: newest_first,
				search: search
			},
			success: (data) => {
				resolve(data);
			}
		});
	});
} // end get_patches()

/**
 * Appends an array of html-string-elements to a html-node.
 * @param list_elem		The node to append the items to.
 * @param html_elems	Array of items to append to the list. (As html strings)
 * @param delay				Animation delay (in ms) for each element appended. Defaults to 100.
 */
function append_to_list(list_elem, html_elems, delay = 100) {
	html_elems.forEach( (list_child, index) => {
		// create DOM node from html string
		list_child = createNodeFromString(list_child);
		list_child.style.animationDelay = delay * index + "ms";
		// append to list
		list_elem.appendChild(list_child);
	});
} // end append_to_list()

/**
 * Handler for patch action listener. Archives or unarchives a patch depending on its current status.
 * @param e Event
 */
function patch_action_listener(e) {
	let that = e.currentTarget;
	if (disabled(that, e)) {
		return;
	}
	let timestamp = that.closest(".patch").dataset.patch;
	let reset = (e.target.closest(".btn").classList.contains("unarchive"));
	archive(timestamp, reset);
} // end patch_action_listener()

/**
 * Archives or resets a patch and moves it to the respective list.
 * @param timestamp	Timestamp/Unique id of the patch.
 * @param reset			When set to false, the patch will be archived. Otherwise it will be reset.
 * @returns {Promise<void>}
 */
async function archive(timestamp, reset) {
	let btn = document.querySelector(".patch[data-patch='" + timestamp + "'] .action .btn");
	// add loading indicator and disable button
	btn.classList.add("loading", "disabled");
	// (un)archive the patch
	let patch = await new Promise( (resolve, reject) => {
		let func = (reset) ? "reset" : "archive";
		$.ajax({
			url: BASE_URL + "panda/patches/" + func + "?lang=" + LANG,
			type: "POST",
			data: {
				timestamp: timestamp
			},
			success: (patch) => {
				resolve(JSON.parse(patch));
			}
		});
	});
	// remove loading indicator and re-enable button
	btn.classList.remove("loading", "disabled");
	// remove the patch from new patches list
	document.querySelector(".patch[data-patch='" + timestamp + "']").remove();
	// add the new element to the other list
	patch = createNodeFromString(patch);
	let destination_list_selector = (reset) ? "#new-patches ul" : "#archive ul.archive-list";
	let destination_list = document.querySelector(destination_list_selector);
	// insert patch at the right position
	let found_position = false;
	Array.from(destination_list.querySelectorAll(".patch")).forEach( (patch_elem) => {
		if (parseInt(timestamp) < parseInt(patch_elem.dataset.patch)) {
			found_position = true;
			destination_list.insertAfter(patch, patch_elem);
		}
	});
	if (!found_position) {
		destination_list.insertAfter(patch, destination_list.firstChild);
	}

	patch.addEventListener("click", function (e) {
		patch_action_listener(e);
	});
} // end archive()

/**
 * Updates and shows the search result preview list.
 * @param search	The current search string, inputted by the user.
 * @returns {Promise<void>}
 */
async function show_search_results(search) {
	let search_results = await new Promise( (resolve, reject) => {
		$.ajax({
			url: BASE_URL + "panda/patches/searchFor?lang=" + LANG,
			type: "POST",
			data: {
				search: search
			},
			success: (result) => {
				resolve(JSON.parse(result).patches);
			}
		});
	});

	let result_list = document.querySelector(".search-result-list ul");
	// empty list
	result_list.innerHTML = "";
	// add search results
	search_results.forEach( (search_result) => {
		let highlighted_code = highlight_difference(search, "Patch-" + search_result.timestamp);
		let highlighted_name = highlight_difference(search, search_result.name);
		let result_elem = "<li data-search='" + search_result.timestamp + "'>" + highlighted_code + "<br/><span class='name'>" + highlighted_name + "</span><div class='status'></div></li>";
		result_elem = createNodeFromString(result_elem);
		result_list.appendChild(result_elem);
	});
	// show list (if not visible already)
	result_list.parentNode.classList.remove("hidden");
} // end show_search_results()

/**
 * Highlights the difference between the original string and the modified string.
 * @param original_string		The original string.
 * @param modified_string		The modified string.
 * @returns {*}	The modified string. Parts that match the original string are highlighted.
 */
function highlight_difference(original_string, modified_string) {
	let highlighted_string = modified_string;
	// split into words
	let words_of_original = original_string.split(" ");
	// and highlight each word individually
	words_of_original.forEach( (word) => {
		highlighted_string = highlighted_string.replace(word, "<span class='highlight'>" + word + "</span>");
	});

	return highlighted_string;
} // end highlight_difference()

/**
 * Gets patches and updates the list.
 * @param type 					Type of db-patches. Either "executed" or "unexecuted" (default).
 * @param replace				When true (default: false) results are appended to the end of the current list.
 * 											When false, list is going to be emptied first.
 */
function update_patches_list(type, replace = false) {
	let load_more_btn 	= document.querySelector(".btn.load-more-results");
	let list_selector 	= (type === "executed") ? "#archive ul.archive-list" : "#new-patches ul";
	let action_selector = (type === "executed") ? ".patch .unarchive" : ".patch .archive";
	let offset 					= (type === "executed" && !replace) ? parseInt(document.querySelector(".load-more-results").dataset.offset) : 0;
	let newest_first 		= (type === "executed") ? true : false;
	let search 					= null;
	if (document.querySelector("input[name='search']").value && document.querySelector("input[name='search']").value !== "") {
		search = document.querySelector("input[name='search']").value;
	}

	if (type === "executed") {
		loading_more = true;
		// add loading indicator to button
		load_more_btn.classList.add("loading", "disabled");
	}

	get_patches(type, offset, newest_first, search).then( (data) => {
		data = JSON.parse(data);

		let patches_list = document.querySelector(list_selector);
		if (replace) {
			patches_list.innerHTML = "";
		}
		append_to_list(patches_list, data.patches);

		// listener for archiving/resetting patch
		Array.from(document.querySelectorAll(action_selector)).forEach(function (element) {
			element.addEventListener("click", function (e) {
				patch_action_listener(e);
			});
		});

		if (type === "executed") {
			// update counters for patches
			total_number_of_patches = data.result_count_total;
			current_number_of_patches += data.result_count;
			load_more_btn.dataset.offset = (offset + data.result_count);
			// remove loading indicator from button and hide it if no more patches to load
			load_more_btn.classList.remove("loading", "disabled");
			if (current_number_of_patches >= total_number_of_patches) {
				load_more_btn.classList.remove("show");
				load_more_btn.classList.add("hidden");
			}
			loading_more = false;
		}
	});
} // end update_patches_list()

/**
 * Handles the "exec_patch_open" event, that indicates that the patch-execution process was started.
 * @param event
 */
function handle_exec_patch_open(event) {
	let data = JSON.parse(event.data);
	document.querySelector("#execute-patches").classList.add("loading", "disabled");
	patch_count = data.patch_count;
	patches_executed = 0;
	progressbar.init("#patch-execution-progress", {
		from: 0,
		to: patch_count,
		steps: (patch_count * 2),
		text_left: "Patch 0/" + patch_count,
		text_right: "0%",
		callback: patches_executed
	});
	Array.from(document.querySelectorAll("#new-patches .patch")).forEach( (elem) => {
		update_patch_elem("waiting", elem);
	});
} // end handle_exec_patch_open()

/**
 * Handles the "exec_patch_close" event, that indicates that the patch-execution process ended.
 * @param event
 */
function handle_exec_patch_close(event) {
	patch_count = 0;
	event_source.close();
	document.querySelector("#execute-patches").classList.remove("loading");
} // end handle_exec_patch_close()

/**
 * Handles the "exec_patch_begin" event, that indicates that the execution of a (single) patch started.
 * @param event
 */
function handle_exec_patch_begin(event) {
	current_patch = JSON.parse(event.data);
	progressbar.advance();
	progressbar.update_text("left", "Patch " + (patches_executed + 1) + "/" + patch_count);
	progressbar.update_text("right", "progress");
	update_patch_elem("running");
} // end handle_exec_patch_begin()

/**
 * Handles the "exec_patch_exception" event, that indicates that there was an error during execution of a (single) patch.
 * @param event
 */
function handle_exec_patch_exception(event) {
	let data = JSON.parse(event.data);
	error_data = {
		message: data.message,
		exception: data.exception.xdebug_message
	};
} // end handle_exec_patch_begin()

/**
 * Handles the "exec_patch_end" event, that indicates that the execution of a (single) patch has ended.
 * @param event
 */
function handle_exec_patch_end(event) {
	let data = JSON.parse(event.data);
	patches_executed++;

	if (data.result === "success") {
		update_patch_elem("success");
		progressbar.advance();
		progressbar.update_text("right", "progress");
	} else if (data.result === "error") {
		update_patch_elem("error");
		progressbar.elem.classList.add("error");
		Array.from(document.querySelectorAll(".patch .status.waiting")).forEach( (waiting_patch_status_elem) => {
			let waiting_patch = waiting_patch_status_elem.closest(".patch");
			update_patch_elem("canceled", waiting_patch);
		});
	}
} // end handle_exec_patch_end()

/**
 * Updates the appearance of a patch element.
 * @param state	State the patch is currently in. One of "new", "waiting", "running", "error", "canceled", "success".
 * @param elem	Optional elem/node of the patch. Defaults to "current" (the patch currently being executed).
 * @returns {boolean} Success.
 */
function update_patch_elem(state, elem = "current") {
	let patch_elem = elem;
	if (patch_elem === "current") {
		patch_elem = document.querySelector(".patch[data-patch='" + current_patch.timestamp + "']");
	}

	let status_elem = patch_elem.querySelector(".status");
	let details_elem = patch_elem.querySelector(".details");

	let status_class;
	let status_text;
	let details;

	switch (state) {
		case "new":
			status_class = "new";
			status_text = "New";
			details = "Click above to execute";
			break;
		case "success":
			status_elem.classList.remove("new");
			status_class = "success";
			status_text = "Success";
			details = "Executed: " + moment().format("DD.MM.YYYY HH:mm");
			break;
		case "error":
			status_class = "error";
			status_text = "Error";
			details = "<a href='javascript:;' class='show-errors bold'>Click to see errors</a>";
			break;
		case "waiting":
			status_class = "waiting";
			status_text = "Wating";
			details = "In queue";
			break;
		case "running":
			status_class = "running";
			status_text = "Running";
			details = "Running";
			break;
		case "canceled":
			status_class = "canceled";
			status_text = "Canceled";
			details = "Canceled";
			break;
		default:
			return false;
	}

	let possible_classes = ["new", "waiting", "running", "error", "canceled", "success"];
	possible_classes.forEach( (possible_class) => {
		status_elem.classList.remove(possible_class);
	});

	status_elem.classList.add(status_class);
	status_elem.querySelector("span").textContent = status_text;
	details_elem.querySelector("span").innerHTML = details;

	if (state === "error") {
		details_elem.querySelector(".show-errors").addEventListener("click", show_last_error);
	}

	return true;
} // end update_patch_elem()

function show_last_error() {
	show_info_modal({
		title: "Errors for Patch-" + current_patch.timestamp,
		text: "<p class='bold'>" + error_data.message + "</p><br/>" + error_data.exception,
		btn_close_text: "Close"
	});
} // end show_last_error()
