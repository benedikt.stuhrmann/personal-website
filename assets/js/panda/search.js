document.addEventListener("DOMContentLoaded", function() {

	// listener for clicking into search input
	document.querySelector("input.search").addEventListener("click", function(e) {
		let search = this.value;
		show_search_results(search);
	});

	// listener for clicking outside search input
	document.addEventListener("click", function(e) {
		if (e.target.closest("input.search") === null && e.target.closest(".search-result-list") === null) {
			close_search_result_list();
		}
	});

	// listener for typing in search input
	document.querySelector("input.search").addEventListener("keyup", function(e) {
		let search = this.value;
		if (search.length > 0) {
			show_search_results(search);
		} else {
			close_search_result_list();
			update_patches_list("executed", true);
		}
	});

	// listener for submitting search input (pressing enter)
	document.querySelector("input.search").addEventListener("keypress", function(e) {
		// if not enter, do nothing
		if (e.keyCode !== 13) return;

		this.blur(); // remove focus from input
		close_search_result_list();
		update_patches_list("executed", true);
	});

	// listener for clicking on search result list item
	document.querySelector(".search-result-list").addEventListener("click", function(e) {
		let clicked_item = e.target.closest("li");
		if (!clicked_item) return;

		let input = document.querySelector("input.search");
		input.value = clicked_item.dataset.search;
		input.blur();
		close_search_result_list();
		update_patches_list("executed", true);
	});

});

/**
 * Closes the search result list.
 * @param empty	If the result list should be emptied when closed (default: false).
 */
function close_search_result_list(empty = false) {
	let search_result_list = document.querySelector(".search-result-list");
	search_result_list.classList.add("hidden");
	if (empty) {
		search_result_list.querySelector("ul").innerHTML = "";
	}
} // close_search_result_list()
