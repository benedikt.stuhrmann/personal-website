document.addEventListener("DOMContentLoaded", function() {

	document.getElementById("login-form").addEventListener("submit", function(e) {
		login_form_submit_handler(e, this);
	});

	document.querySelector(".password-visibility").addEventListener("click", function(e) {
		password_visibility_handler(e, this);
	});

}, false); // end DOMContentLoaded

/**
 * Called when login form gets submitted. Handles login process.
 * @param e 	 Submit event.
 * @param that Form element.
 */
function login_form_submit_handler(e, that) {
	e.preventDefault();

	// remove errors
	that.querySelector(".error-container .invalid").classList.add("hidden");
	that.querySelector(".error-container .server-error").classList.add("hidden");

	// validate form
	if (!$(that).parsley().isValid()) {
		return;
	}

	// disable button and add loading indicator
	that.querySelector(".btn[name='login']").classList.add("loading");
	that.querySelector(".btn[name='login']").classList.add("disabled");

	// read form-inputs
	let username = that.querySelector("input[name='username']").value;
	let password = that.querySelector("input[name='password']").value;

	// try login
	$.when(try_login(username, password)).done(function (result) {
		result = JSON.parse(result);

		if (result === true) {
			window.location.href = BASE_URL + "panda/dashboard";
		} else {
			// reset parsley validation
			$(that).parsley().reset();
			bulk_change_classes(that, "form", ".icon", ["parsley-error", "parsley-success"], "remove");
			// enable login button and remove loading indicator
			that.querySelector(".btn[name='login']").classList.remove("loading");
			that.querySelector(".btn[name='login']").classList.remove("disabled");

			if (result === false) {
				that.querySelector(".error-container .invalid").classList.remove("hidden");
			} else {
				that.querySelector(".error-container .server-error").classList.remove("hidden");
			}
		}
	});
} // end login_form_submit_handler()

/**
 * Tries to login a user with a username-password combination.
 * @param {string} username The username to login with.
 * @param {string} password	The password to login with.
 * @returns {*|jQuery|{getAllResponseHeaders, abort, setRequestHeader, readyState, getResponseHeader, overrideMimeType, statusCode}}
 */
function try_login(username, password) {
	return $.ajax({
		url: BASE_URL + "panda/login/login",
		type: "POST",
		data: {
			username: username,
			password: password
		}
	});
} // end login()

/**
 * Called when password visibility button gets clicked.
 * @param e 	 Click event.
 * @param that Button element.
 */
function password_visibility_handler(e, that) {
	let password_input = document.querySelector("input[name='password']");
	if (password_input.type === "password") {
		password_input.type = "text";
	} else {
		password_input.type = "password";
	}
} // end password_visibility_handler()
