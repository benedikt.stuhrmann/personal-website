let progressbar = {
	elem: null,
	fill_elem: null,
	text_elem_left: null,
	text_elem_right: null,
	default_options: {
		from: 0,
		to: 100,
		steps: 100,
		start: 0,
		style: {
			width : "100%",
			height : "40px",
			background_fill : "#F6F6F6",
			progress_fill : "#78BCC4"
		},
		text_left: null,
		text_right: null,
		callback: null
	},
	options: null,
	init: function (selector, options = {}) {
		if (!selector || typeof selector !== "string") {
			console.error("Progress bar element missing. Please specify a valid selector.");
			return false;
		}
		this.elem = document.querySelector(selector);
		this.options = {...this.default_options, ...options};
		this.create();
	},
	create: function () {
		this.elem.classList.add("progressbar-bg");
		this.elem.style.width = this.options.style.width;
		this.elem.style.height = this.options.style.height;
		this.elem.style.backgroundColor = this.options.style.background_fill;

		let fill_elem = this.elem.querySelector("div.progressbar-fill");
		if (fill_elem) {
			this.fill_elem = fill_elem;
		} else {
			this.fill_elem = document.createElement("div");
			this.fill_elem.classList.add("progressbar-fill");
			this.elem.appendChild(this.fill_elem);
		}
		this.fill_elem.style.backgroundColor = this.options.style.progress_fill;

		let text_elem_left = this.elem.querySelector(".progressbar-text-left");
		if (text_elem_left) {
			this.text_elem_left = text_elem_left;
		} else {
			this.text_elem_left = document.createElement("span");
			this.text_elem_left.classList.add("progressbar-text", "progressbar-text-left");
			this.elem.appendChild(this.text_elem_left);
		}
		this.text_elem_left.innerHTML = this.options.text_left || "";

		let text_elem_right = this.elem.querySelector(".progressbar-text-right");
		if (text_elem_right) {
			this.text_elem_right = text_elem_right;
		} else {
			this.text_elem_right = document.createElement("span");
			this.text_elem_right.classList.add("progressbar-text", "progressbar-text-right");
			this.fill_elem.appendChild(this.text_elem_right);
		}
		this.text_elem_right.innerHTML = this.options.text_right || "";

		this.calc_step_size();
	},
	calc_step_size : function () {
		this.step_size = (this.elem.offsetWidth / (this.options.to - this.options.from));
		this.current_width = this.options.start == 0 ? this.step_size : (this.step_size * this.options.start);
		this.current_step = this.options.start;
	},
	set_progress: function (progress) {
		this.fill_elem.style.width = (progress * this.step_size) + "px";
		this.check_progress();
	},
	advance: function (factor = 1) {
		if (this.current_step < this.options.steps) {
			let step_size = (this.options.to - this.options.from) / this.options.steps;
			this.current_step += (step_size * factor);
			this.fill_elem.style.width = (this.current_step * this.step_size) + "px";
		}
		this.check_progress();
	},
	check_progress: function () {
		if (this.current_step === this.options.steps) {
			if (this.options.callback) {
				this.options.callback();
			}
		}
	},
	update_text: function(position, text) {
		if (position === "left" && this.text_elem_left) {
			this.text_elem_left.innerHTML = text;
		}
		if (position === "right" && this.text_elem_right) {
			if (text === "progress") {
				let perc = Math.floor((this.current_step / this.options.to) * 100);
				text = perc + "%";
			}

			this.text_elem_right.innerHTML = text;
		}
	}
};
