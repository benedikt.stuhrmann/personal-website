function typingAnimation(elem) {
  chars = prepareDom(elem);
  type(elem);
} // end typingAnimation

/**
 * Current offset in the character array
 * @type {number}
 */
let n = 0;
/**
 * Character array for typing animation.
 */
let chars;

/**
 * Adds a letter to the visible string. Moves cursor.
 * @param elem DOM-Element for the typing animation
 */
function type(elem) {
  $cursor = $(elem).find(".cursor");
  $letter_spans = $(elem).find(".letter");

  if (n < $letter_spans.length) {
    let letter_span = $letter_spans[n];
    $(letter_span).removeClass("invisible");
    $cursor.insertAfter($(letter_span));
    n++;
    if (n == $letter_spans.length) {
      typingAnimationDone();
    }
    setTimeout(function() {
      type(elem);
    }, getTimeout(n));
  } else {
    setTimeout(function() {
      $cursor.removeClass("play");
      $cursor.addClass("fade-out");
    }, 2000);
  }
} // end type()

/**
 * Determines the timeout by depending on the letter value.
 * @param n Index in char array
 * @returns {number} timeout
 */
function getTimeout(n) {
  let timeout = 0;
  switch (chars[n-1]) {
    case " ": timeout = 20;
      break;
    case ".": timeout = 1200;
      break;
    default:
      timeout = 60;
  }

  return timeout;
} // end getTimeout()

/**
 * Prepares the DOM for the typing animation.
 * Converts the text into letter-spans and adds the cursor.
 * @param elem DOM-Element for the animation.
 * @returns {string[]} Array of characters for the typing animation.
 */
function prepareDom(elem) {
  let original_content = $(elem).html();

  let result = prepareString(original_content);

  let chars = result.string.split("");
  let html = "<span class='cursor play'></span>";
  for (let i = 0; i < chars.length; i++) {
    if (result.breaks.includes(i)) {
      html += "<br/>";
    }

    html += "<span class='letter invisible'>" + chars[i] + "</span>";
  }

  $(elem).html(html);

  return chars;
} // end prepareDom()

/**
 * Removes linebreaks from the string.
 * @param original_string The original string.
 * @returns {{string: (string), breaks: Array}} "string" holds the original string without linebreaks.
 *                                              "breaks" is an array with the index of linebreaks
 */
function prepareString(original_string) {
  let string = original_string;

  // remove linebreaks from the string and store them for later
  let breaks = [];
  let found_one = false;
  do {
    // check for <br/>
    let index = string.indexOf("<br/>");
    if (index > -1) {
      found_one = true;
      breaks.push(index);
      string = string.replace("<br/>", "");
    } else {
      // if not found, check for <br>
      index = string.indexOf("<br>");
      if (index > -1) {
        found_one = true;
        breaks.push(index);
        string = string.replace("<br>", "");
      } else {
        // if not found, no more linebreaks
        found_one = false;
      }
    }
  } while(found_one);

  return {
    string: string,
    breaks: breaks
  }
} // end prepareString()