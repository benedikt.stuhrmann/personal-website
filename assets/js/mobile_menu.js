$(document).ready( () => {
	// menu
	$("#hamburger-menu").on("click", function() {
		$(this).toggleClass("menu-open");
		$("nav").toggleClass("menu-open");
	});

	$("nav ul li").on("click", function() {
		$("#hamburger-menu").removeClass("menu-open");
		$("nav").removeClass("menu-open");
	});

	// Swiping for menu open-close.
	document.addEventListener("touchstart", handleTouchStart, false);
	document.addEventListener("touchmove", handleTouchMove, false);
}); // end ready()

/* Swiping detection */
let x_down = null;
let y_down = null;

function getTouches(evt) {
	return evt.touches || // browser API
		evt.originalEvent.touches; // jQuery
} // end getTouches()

function handleTouchStart(evt) {
	const firstTouch = getTouches(evt)[0];
	x_down = firstTouch.clientX;
	y_down = firstTouch.clientY;
} // end handleTouchStart()

function handleTouchMove(evt) {
	if ( ! x_down || ! y_down ) {
		return;
	}

	let x_up = evt.touches[0].clientX;
	let y_up = evt.touches[0].clientY;

	let x_diff = x_down - x_up;
	let y_diff = y_down - y_up;

	if (x_diff > 8 && x_diff > Math.abs(y_diff)) {
		// swipe from right
		$("#hamburger-menu").addClass("menu-open");
		$("nav").addClass("menu-open");
	} else if (x_diff < -8 && Math.abs(x_diff) > Math.abs(y_diff)) {
		// swipe from left
		if (!$("#hamburger-menu").hasClass("menu-open")) {
			$("#hamburger-menu").addClass("menu-open");
			$("nav").addClass("menu-open");
		} else {
			$("#hamburger-menu").removeClass("menu-open");
			$("nav").removeClass("menu-open");
		}
	}

	// reset values
	x_down = null;
	y_down = null;
} // end handleTouchMove()
