<?php
namespace DataObjects;

defined('BASEPATH') || exit('No direct script access allowed');


class ErrorObj extends DataObj {

  /**
   * @var string The error message.
   */
  private $message;


  /**
   * ErrorObj constructor.
   * @param array|null $attrs Array of error attributes where the keys are the attribute-names.
   */
  public function __construct(array $attrs = null) {
    parent::__construct();

    if ($attrs) $this->fromArray($attrs);
  } // end __construct()

  /**
   * Sets the error objects attributes with array values.
   * @param array $attrs Array of error attributes where the keys are the attribute-names.
   */
  public function fromArray(array $attrs) {
    parent::fromArray($attrs);

    if (array_key_exists("message", $attrs))   $this->setMessage($attrs["message"]);
  } // end fromArray()


  /**
   * @return string
   */
  public function getMessage(): string {
    return $this->message;
  } // end getMessage()

  /**
   * @param string $message
   */
  public function setMessage(string $message): void {
    $this->message = $message;
  } // end setMessage()

} // end class
