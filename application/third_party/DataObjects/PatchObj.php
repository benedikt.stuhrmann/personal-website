<?php
namespace DataObjects;

use DateTime;

defined('BASEPATH') || exit('No direct script access allowed');


class PatchObj extends DataObj {

	/**
	 * Constants for determining a patches type.
	 */
	const EXECUTED = "executed";
	const UNEXECUTED = "unexecuted";
	const ALL = "all";

  /**
   * @var int Timestamp of patch creation. Used as primary key.
   */
  private $timestamp;

	/**
	 * @var string Name of the patch, for easier recognition.
	 */
	private $name;

	/**
	 * @var DateTime When the patch was executed. NULL if not executed.
	 */
	private $executed;


  /**
   * PatchObj constructor.
   * @param array|null $attrs Array of language attributes where the keys are the attribute-names.
   */
  public function __construct(array $attrs = null) {
    parent::__construct();

    if ($attrs) $this->fromArray($attrs);
  } // end __construct()

  /**
   * Sets the patch objects attributes with array values.
   * @param array $attrs Array of patch attributes where the keys are the attribute-names.
   */
  public function fromArray(array $attrs) {
    parent::fromArray($attrs);

    if (array_key_exists("timestamp", $attrs))  $this->setTimestamp($attrs["timestamp"]);
		if (array_key_exists("name", $attrs))   		$this->setName($attrs["name"]);
		if (array_key_exists("executed", $attrs))   $this->setExecuted($attrs["executed"]);
  } // end fromArray()


  /**
   * @return int
   */
  public function getTimestamp() : int {
    return $this->timestamp;
  } // end getTimestamp()

  /**
   * @param int $timestamp
   */
  public function setTimestamp(int $timestamp) : void {
    $this->timestamp = $timestamp;
  } // end setTimestamp()

	/**
	 * @return string
	 */
	public function getName() : string {
		return $this->name;
	} // end getName()

	/**
	 * @param string $name
	 */
	public function setName(string $name) : void {
		$this->name = $name;
	} // end setName()

	/**
	 * @return DateTime|null
	 */
	public function getExecuted() {
		return $this->executed;
	} // end getExecuted()

	/**
	 * @param DateTime|string $executed
	 */
	public function setExecuted($executed): void {
		if (is_string($executed)) {
			$this->executed = new DateTime($executed);
		} else if ($executed instanceof DateTime) {
			$this->executed = $executed;
		}
	} // end setExecuted()

	/**
	 * Gets the objects data as an array, that can directly be inserted into the database.
	 * @return array Key-value array to be inserted into the database.
	 */
	public function getInsertData() : array {
		$insert_data = [
			"timestamp" => $this->getTimestamp(),
			"name" 			=> $this->getName(),
			"executed"  => $this->getExecuted()
		];
		return $insert_data;
	} // end getInsertData()

	public static function validType(string $type) : bool {
		return ($type === self::EXECUTED || $type === self::UNEXECUTED || $type === self::ALL);
	} // end validType()

	/**
	 * Specify data which should be serialized to JSON
	 * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize() {
		$data = parent::jsonSerialize();
		$patch_data = [
			"timestamp" => $this->getTimestamp(),
			"name" 			=> $this->getName(),
			"executed" 	=> $this->getExecuted()
		];
		return array_merge($data, $patch_data);
	} // end jsonSerialize()

} // end class
