<?php
namespace DataObjects;

defined('BASEPATH') || exit('No direct script access allowed');

use JsonSerializable;
use DateTime;

class DataObj implements JsonSerializable {

  /**
   * @var CI
   */
  private $CI;

  /**
   * @var int Unique id.
   */
  private $id;

  /**
   * @var DateTime Creation date.
   */
  private $created;

  /**
   * @var UserObj Creator.
   */
  private $creator;

  /**
   * @var DateTime Last edit date.
   */
  private $last_edited;

  /**
   * @var UserObj Last editor.
   */
  private $editor;


  /**
   * DataObj constructor.
   * @param array|null $attrs Array of data-obj attributes where the keys are the attribute-names.
   */
  public function __construct(array $attrs = null) {
    $this->CI = get_instance();

    if ($attrs) $this->fromArray($attrs);
  } // end __construct()

  /**
   * Sets the data objects attributes with array values.
   * @param array $attrs Array of data-obj attributes where the keys are the attribute-names.
   */
  public function fromArray(array $attrs) {
    if (array_key_exists("id", $attrs))           $this->setId($attrs["id"]);
    if (array_key_exists("created", $attrs))      $this->setCreated($attrs["created"]);
    if (array_key_exists("creator", $attrs))      $this->setCreator($attrs["creator"]);
    if (array_key_exists("last_edited", $attrs))  $this->setLastEdited($attrs["last_edited"]);
    if (array_key_exists("editor", $attrs))       $this->setEditor($attrs["editor"]);
  } // end fromArray()


  /**
   * @return int|null
   */
  public function getId() {
    return $this->id;
  } // end getId

  /**
   * @param int $id
   */
  public function setId(int $id): void {
    $this->id = $id;
  } // end setId

  /**
   * @return DateTime|null
   */
  public function getCreated() {
    return $this->created;
  } // end getCreated()

  /**
   * @param $created
   */
  public function setCreated($created): void {
    if (is_string($created)) {
      $this->created = new DateTime($created);
    } else if ($created instanceof DateTime) {
      $this->created = $created;
    }
  } // end setCreated()

  /**
   * @return UserObj|null
   */
  public function getCreator() {
    return $this->creator;
  }// end getCreator()

  /**
   * @param $creator
   */
  public function setCreator($creator): void {
    if ($creator instanceof UserObj) {
      $this->creator = $creator;
    } else if (is_numeric($creator)) {
      $this->creator = $this->CI->user_model->get(["id" => intval($creator)]);
    }
  }// end setCreator()

  /**
   * @return DateTime|null
   */
  public function getLastEdited() {
    return $this->last_edited;
  }// end getLastEdited()

  /**
   * @param $last_edited
   */
  public function setLastEdited($last_edited): void {
    if (is_string($last_edited)) {
      $this->last_edited = new DateTime($last_edited);
    } else if ($last_edited instanceof DateTime) {
      $this->last_edited = $last_edited;
    }
  }// end setLastEdited()

  /**
   * @return UserObj|null
   */
  public function getEditor() {
    return $this->editor;
  } // end getEditor()

  /**
   * @param $editor
   */
  public function setEditor($editor): void {
    if ($editor instanceof UserObj) {
      $this->editor = $editor;
    } else if (is_numeric($editor)) {
      $this->editor = $this->CI->user_model->get(["id" => intval($editor)]);
    }
  } // end setEditor()

	/**
	 * Gets the objects data as an array, that can directly be inserted into the database.
	 * @return array Key-value array to be inserted into the database.
	 */
	public function getInsertData() : array {
		$insert_data = [
			"id" 					=> $this->getId(),
			"created" 		=> $this->getCreated(),
			"creator" 		=> $this->getCreator(),
			"last_edited" => $this->getLastEdited(),
			"editor" 			=> $this->getEditor(),
		];
		return $insert_data;
	} // end getInsertData()

	/**
	 * Specify data which should be serialized to JSON
	 * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize() {
		$data = [
			"id" 					=> $this->getId(),
			"created" 		=> $this->getCreated(),
			"creator" 		=> $this->getCreator(),
			"last_edited" => $this->getLastEdited(),
			"editor" 			=> $this->getEditor()
		];
		return $data;
	} // end jsonSerialize()

} // end class
