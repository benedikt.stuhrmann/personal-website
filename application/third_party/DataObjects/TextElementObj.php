<?php
namespace DataObjects;

defined('BASEPATH') || exit('No direct script access allowed');


class TextElementObj extends DataObj {

  /**
   * @var string Text element code.
   */
  private $code;

  /**
   * TextElementObj constructor.
   * @param array|null $attrs Array of text-element attributes where the keys are the attribute-names.
   */
  public function __construct(array $attrs = null) {
    parent::__construct();

    if ($attrs) $this->fromArray($attrs);
  } // end __construct()

  /**
   * Sets the text-element objects attributes with array values.
   * @param array $attrs Array of text-element attributes where the keys are the attribute-names.
   */
  public function fromArray(array $attrs) {
    parent::fromArray($attrs);

    if (array_key_exists("code", $attrs))   $this->setCode($attrs["code"]);
  } // end fromArray()


  /**
   * @return string
   */
  public function getCode(): string {
    return $this->code;
  } // end getCode()

  /**
   * @param string $code
   */
  public function setCode(string $code): void {
    $this->code = $code;
  } // end setCode()

} // end class