<?php
namespace DataObjects;

defined('BASEPATH') || exit('No direct script access allowed');


class LanguageObj extends DataObj {

	const LANG_EN = 1;
	const LANG_GER = 2;

  /**
   * @var string Language code.
   */
  private $lang_code;


  /**
   * LanguageObj constructor.
   * @param array|null $attrs Array of language attributes where the keys are the attribute-names.
   */
  public function __construct(array $attrs = null) {
    parent::__construct();

    if ($attrs) $this->fromArray($attrs);
  } // end __construct()

  /**
   * Sets the language objects attributes with array values.
   * @param array $attrs Array of language attributes where the keys are the attribute-names.
   */
  public function fromArray(array $attrs) {
    parent::fromArray($attrs);

    if (array_key_exists("lang_code", $attrs))   $this->setLangCode($attrs["lang_code"]);
  } // end fromArray()


  /**
   * @return string
   */
  public function getLangCode(): string {
    return $this->lang_code;
  } // end getLangCode()

  /**
   * @param string $lang_code
   */
  public function setLangCode(string $lang_code): void {
    $this->lang_code = $lang_code;
  } // end setLangCode()

} // end class
