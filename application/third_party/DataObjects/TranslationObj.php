<?php
namespace DataObjects;

defined('BASEPATH') || exit('No direct script access allowed');


class TranslationObj extends DataObj {

  /**
   * @var TextElementObj Text Element Object.
   */
  private $text_element;

  /**
   * @var LanguageObj Language Object
   */
  private $language;

  /**
   * @var string The text.
   */
  private $text;


  /**
   * TranslationObj constructor.
   * @param array|null $attrs Array of translation attributes where the keys are the attribute-names.
   */
  public function __construct(array $attrs = null) {
    parent::__construct();

    if ($attrs) $this->fromArray($attrs);
  } // end __construct()

  /**
   * Sets the translation objects attributes with array values.
   * @param array $attrs Array of translation attributes where the keys are the attribute-names.
   */
  public function fromArray(array $attrs) {
    parent::fromArray($attrs);

    if (array_key_exists("text_element", $attrs))   $this->setTextElement($attrs["text_element"]);
    if (array_key_exists("language", $attrs)) $this->setLanguage($attrs["language"]);
    if (array_key_exists("text", $attrs))  $this->setText($attrs["text"]);
  } // end fromArray()


  /**
   * @return TextElementObj
   */
  public function getTextElement(): TextElementObj {
    return $this->text_element;
  } // end getTextElement()

  /**
   * @param TextElementObj $text_element
   */
  public function setTextElement(TextElementObj $text_element): void {
    $this->text_element = $text_element;
  } // end setTextElement()

  /**
   * @return LanguageObj
   */
  public function getLanguage(): LanguageObj {
    return $this->language;
  } // end getLanguage()

  /**
   * @param LanguageObj $language
   */
  public function setLanguage(LanguageObj $language): void {
    $this->language = $language;
  } // end setLanguage()

  /**
   * @return string
   */
  public function getText(): string {
    return $this->text;
  } // end getText()

  /**
   * @param string $text
   */
  public function setText(string $text): void {
    $this->text = $text;
  } // end setText()

} // end class