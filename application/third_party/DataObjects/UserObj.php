<?php
namespace DataObjects;

defined('BASEPATH') || exit('No direct script access allowed');


class UserObj extends DataObj {

	/**
	 * @var int If of the bestuh app user.
	 */
	const APP_USER = 2;

  /**
   * @var string Username.
   */
  private $username;

  /**
   * @var string First name of the user.
   */
  private $first_name;

  /**
   * @var string Last name of the user.
   */
  private $last_name;

  /**
   * @var string Users password.
   */
  private $password;


  /**
   * UserObj constructor.
   * @param array|null $attrs Array of user attributes where the keys are the attribute-names.
   */
  public function __construct(array $attrs = null) {
    parent::__construct();

    if ($attrs) $this->fromArray($attrs);
  } // end __construct()

  /**
   * Sets the user objects attributes with array values.
   * @param array $attrs Array of user attributes where the keys are the attribute-names.
   */
  public function fromArray(array $attrs) {
    parent::fromArray($attrs);

    if (array_key_exists("username", $attrs))   $this->setUsername($attrs["username"]);
    if (array_key_exists("first_name", $attrs)) $this->setFirstName($attrs["first_name"]);
    if (array_key_exists("last_name", $attrs))  $this->setLastName($attrs["last_name"]);
    if (array_key_exists("password", $attrs))   $this->setPassword($attrs["password"]);
  } // end fromArray()


  /**
   * @return string
   */
  public function getUsername() : string {
    return $this->username;
  } // end getUsername()

  /**
   * @param string $username
   */
  public function setUsername(string $username) : void {
    $this->username = $username;
  } // end setUsername()

  /**
   * @return string
   */
  public function getFirstName() : string {
    return $this->first_name;
  } // end getFirstName()

  /**
   * @param string $first_name
   */
  public function setFirstName(string $first_name) : void {
    $this->first_name = $first_name;
  } // end setFirstName()

  /**
   * @return string
   */
  public function getLastName() : string {
    return $this->last_name;
  } // end getLastName()

  /**
   * @param string $last_name
   */
  public function setLastName(string $last_name) : void {
    $this->last_name = $last_name;
  } // end setLastName()

  /**
   * @return string
   */
  public function getPassword() : string {
    return $this->password;
  } // end getPassword()

  /**
   * @param string $password
   */
  public function setPassword(string $password) : void {
    $this->password = $password;
  } // end setPassword()

} // end class
