<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public function __construct() {
    parent::__construct();

  } // end __construct()

  public function index() {
    $this->load->view("frame", [
      "view"   => "index",
      "title"  => "Benedikt Stuhrmann",
      "anchor" => "home",
      "device" => getDevice()
    ]);
  } // end index()

} // end class