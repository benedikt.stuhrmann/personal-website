<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Projects controller.
 * @property-read Authenticator authenticator
 */
class Projects extends CI_Controller {

	/**
	 * Projects constructor.
	 */
	public function __construct() {
		parent::__construct();
		$this->authenticator->checkAccess();
		$this->config->load("navigation");
	} // end __construct()

	public function index() {
		$this->load->view("panda/frame", [
			"view"   				=> "panda",
			"page"   				=> "projects",
			"title"  				=> $this->translator->get("window_title_projects"),
			"device" 				=> getDevice(),
			"page_title" 	  => $this->translator->get("page_title_projects"),
			"page_subtitle" => $this->translator->get("page_subtitle_projects"),
			"nav_items"			=> $this->config->item("navigation")
		]);
	} // end index()

} // end class
