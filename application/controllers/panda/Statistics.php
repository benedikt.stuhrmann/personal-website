<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Statistics controller.
 * @property-read Authenticator authenticator
 */
class Statistics extends CI_Controller {

	/**
	 * Statistics constructor.
	 */
	public function __construct() {
		parent::__construct();
		$this->authenticator->checkAccess();
		$this->config->load("navigation");
	} // end __construct()

	public function index() {
		$this->load->view("panda/frame", [
			"view"   				=> "panda",
			"page"   				=> "statistics",
			"title"  				=> $this->translator->get("window_title_statistics"),
			"device" 				=> getDevice(),
			"page_title" 	  => $this->translator->get("page_title_statistics"),
			"page_subtitle" => $this->translator->get("page_subtitle_statistics"),
			"nav_items"			=> $this->config->item("navigation")
		]);
	} // end index()

} // end class
