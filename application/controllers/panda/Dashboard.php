<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Dashboard controller.
 * @property-read Authenticator authenticator
 */
class Dashboard extends CI_Controller {

	/**
	 * Dashboard constructor.
	 */
	public function __construct() {
		parent::__construct();
		$this->authenticator->checkAccess();
		$this->config->load("navigation");
	} // end __construct()

	public function index() {
		$this->load->view("panda/frame", [
			"view"   				=> "panda",
			"page"   				=> "dashboard",
			"page_layout"			=> "fluid",
			"title"  				=> $this->translator->get("window_title_dashboard"),
			"device" 				=> getDevice(),
			"page_title" 	  		=> $this->translator->get("page_title_dashboard"),
			"page_subtitle" 		=> $this->translator->get("page_subtitle_dashboard"),
			"nav_items"				=> $this->config->item("navigation")
		]);
	} // end index()

} // end class
