<?php
defined("BASEPATH") OR exit("No direct script access allowed");

use DataObjects\PatchObj;

/**
 * Patches controller.
 * @property-read Authenticator authenticator
 * @property-read Patcher patcher
 */
class Patches extends CI_Controller {

	/**
	 * @var int Patches to load per page.
	 */
	private const RESULTS_PER_PAGE = 10;

	/**
	 * Patches constructor.
	 */
	public function __construct() {
		parent::__construct();
		$this->authenticator->checkAccess();
		$this->config->load("navigation");
	} // end __construct()

	public function index() {
		$this->patcher->checkForPatches();
		$this->load->view("panda/frame", [
			"view"   					 => "panda",
			"page"   					 => "patches",
			"title"  					 => $this->translator->get("window_title_patches"),
			"device" 					 => getDevice(),
			"page_title" 	  	 => $this->translator->get("page_title_patches"),
			"page_subtitle" 	 => $this->translator->get("page_subtitle_patches"),
			"nav_items"				 => $this->config->item("navigation"),
			"results_per_page" => self::RESULTS_PER_PAGE
		]);
	} // end index()

	public function getPatches() {
		$this->authenticator->ifOtherThanAjax();
		$type = $this->input->post("type");
		$offset = intval($this->input->post("offset"));
		$newest_first = $this->input->post("newest_first") === "true" ? true : false;
		$search = $this->input->post("search");
		$data = $this->patcher->getPatches($type, $search, 10, $offset, $newest_first);

		$patch_elems = [];
		foreach ($data["patches"] as $patch) {
			$patch_elems[] = $this->load->view("panda/layout/patch", [
				"timestamp" => $patch->getTimestamp(),
				"name" => $patch->getName(),
				"executed" => $patch->getExecuted()
			], true);
		}

		$data = [
			"patches" => $patch_elems,
			"result_count" => sizeof($patch_elems),
			"result_count_total" => $data["total"]
		];

		echo json_encode($data);
	} // end getPatches()

	public function searchFor() {
		$this->authenticator->ifOtherThanAjax();
		$search = $this->input->post("search");
		$search_results = $this->patcher->getPatches(PatchObj::EXECUTED, $search);
		echo json_encode($search_results);
	} // end searchFor()

	/**
	 * Archives a specified patch.
	 * @param bool $reset When set to true (default: false), the patch will be removed from the archive instead of being added to it.
	 * Echoes the json encoded patch on success. Otherwise false.
	 */
	public function archive(bool $reset = false) {
		$this->authenticator->ifOtherThanAjax();
		$timestamp = intval($this->input->post("timestamp"));
		$success = $this->patcher->archive($timestamp, $reset);
		if ($success) {
			$patch = $this->patcher->getPatch(["timestamp" => $timestamp]);
			$patch = $this->load->view("panda/layout/patch", [
				"timestamp" => $patch->getTimestamp(),
				"name" => $patch->getName(),
				"executed" => $patch->getExecuted()
			], true);
			echo json_encode($patch);
		} else {
			echo json_encode(false);
		}
	} // end archive()

	/**
	 * Removes a patch from the archive.
	 */
	public function reset() {
		$this->authenticator->ifOtherThanAjax();
		$this->archive(true);
	} // end reset()

	/**
	 * Executes new patches. This should be called by a javascript EventSource object to function correctly.
	 * The special output format can be found here:
	 * EventSource documentation: https://developer.mozilla.org/de/docs/Web/API/EventSource
	 * Server-Side Events: https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events
	 */
	public function execute() {
		header("Content-Type: text/event-stream");
		set_time_limit(0);
		$unexecuted_patches = $this->patcher->getPatches(PatchObj::UNEXECUTED, null, 99999, 0, false);

		echo "event: exec_patch_open" . PHP_EOL;
		echo "data: " . json_encode(["patch_count" => $unexecuted_patches["total"]]) . PHP_EOL . PHP_EOL;

		$error = false;

		foreach ($unexecuted_patches["patches"] as $patch) {
			echo "event: exec_patch_begin" . PHP_EOL;
			echo "data: " . json_encode($patch) . PHP_EOL . PHP_EOL;

			try {
				$this->patcher->executePatch($patch->getTimestamp());
			} catch (Throwable $exception) {
				echo "event: exec_patch_exception" . PHP_EOL;
				echo "data: " . json_encode([
					"exception" => $exception,
					"message" 	=> "An exception occurred with message '" . $exception->getMessage() . "'" . PHP_EOL . " at: " . $exception->getFile() . ":" . $exception->getLine()
					]) . PHP_EOL . PHP_EOL;

				$error = true;
			}

			echo "event: exec_patch_end" . PHP_EOL;
			echo "data: " . json_encode(["result" => $error ? "error" : "success"]) . PHP_EOL . PHP_EOL;

			if ($error) {
				break;
			}
		}

		echo "event: exec_patch_close" . PHP_EOL;
		echo "data:" . PHP_EOL . PHP_EOL;
	} // end execute()

} // end class
