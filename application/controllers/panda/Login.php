<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Login controller.
 * @property-read Users users
 * @property-read Authenticator authenticator
 */
class Login extends CI_Controller {

	/**
	 * Login constructor.
	 */
	public function __construct() {
		parent::__construct();
		$this->authenticator->checkAccess();
	} // end __construct()

	public function index() {
		$this->load->view("panda/frame", [
			"view"   => "login",
			"title"  => "PANDA - Login",
			"device" => getDevice()
		]);
	} // end index()

	/**
	 * Tries to login the user with the passed username and password.
	 * Only ajax-post requests are allowed.
	 * Request data has to contain "username" and "password".
	 * Echoes json encoded
	 * - true, on successful login.
	 * - false, if username and/or password are incorrect.
	 * - "error", if something went wrong.
	 */
	public function login() {
		$this->authenticator->ifOtherThanAjax("panda/login");

		$username = $this->input->post("username");
		$password = $this->input->post("password");

		try {
			$success = $this->users->login($username, $password);
			$response = $success;
		} catch (Exception $e) {
			$response = "error";
			$this->CI->logger->logError("Unable to login: {$e->getMessage()}", false);
		}

		echo json_encode($response);
	} // end login()

	/**
	 * Tries to logout the current user.
	 * Only ajax-post requests are allowed.
	 * Echoes json encoded
	 * - true, on successful logout.
	 * - false, on unsuccessful logout.
	 * - "error", if something went wrong.
	 */
	public function logout() {
		$this->authenticator->ifOtherThanAjax();

		try {
			$success = $this->users->logout();
			$response = $success;
		} catch (Exception $e) {
			$response = "error";
			$this->CI->logger->logError("Unable to logout: {$e->getMessage()}", false);
		}

		echo json_encode($response);
	} // end logout()

} // end class
