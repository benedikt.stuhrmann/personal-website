<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Texts controller.
 * @property-read Authenticator authenticator
 */
class Texts extends CI_Controller {

	/**
	 * Texts constructor.
	 */
	public function __construct() {
		parent::__construct();
		$this->authenticator->checkAccess();
		$this->config->load("navigation");
	} // end __construct()

	public function index() {
		$this->load->view("panda/frame", [
			"view"   				=> "panda",
			"page"   				=> "texts",
			"title"  				=> $this->translator->get("window_title_texts"),
			"device" 				=> getDevice(),
			"page_title" 	  => $this->translator->get("page_title_texts"),
			"page_subtitle" => $this->translator->get("page_subtitle_texts"),
			"nav_items"			=> $this->config->item("navigation")
		]);
	} // end index()

} // end class
