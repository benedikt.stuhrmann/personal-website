<?php

$config['a'] = 'b';

/**
 * Default setting for showing errors.
 * True when in development mode, otherwise false.
 */
DEFINE('SHOW_ERRORS', ENVIRONMENT === 'development');

/**
 * Default setting for showing warnings.
 * True when in development mode, otherwise false.
 */
DEFINE('SHOW_WARNINGS', ENVIRONMENT === 'development');

/**
 * Hash algorithm to use for hashing passwords.
 */
DEFINE('PASSWORD_HASH_ALGORITHM', PASSWORD_DEFAULT);

/**
 * Main database name.
 */
DEFINE('DB_BESTUH', "bestuhdb");

/**
 * Directory for database patches..
 */
DEFINE('PATCHES_DIR', FCPATH . "application" . DIRECTORY_SEPARATOR . "patches" . DIRECTORY_SEPARATOR);

