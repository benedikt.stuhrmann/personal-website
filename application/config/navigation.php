<?php

$config["navigation"] = [
	(object) [
		"name"	=> "dashboard",
		"icon"	=> file_get_contents(base_url() . "assets/icons/dashboard.svg"),
		"link"	=> base_url() . "panda/dashboard",
		"classes" => []
	],
	(object) [
		"name"	=> "statistics",
		"icon"	=> file_get_contents(base_url() . "assets/icons/chart.svg"),
		"link"	=> base_url() . "panda/statistics",
		"classes" => []
	],
	(object) [
		"name"	=> "projects",
		"icon"	=> file_get_contents(base_url() . "assets/icons/list.svg"),
		"link"	=> base_url() . "panda/projects",
		"classes" => []
	],
	(object) [
		"name"	=> "texts",
		"icon"	=> file_get_contents(base_url() . "assets/icons/text.svg"),
		"link"	=> base_url() . "panda/texts",
		"classes" => []
	],
	(object) [
		"name"	=> "patches",
		"icon"	=> file_get_contents(base_url() . "assets/icons/db-patch.svg"),
		"link"	=> base_url() . "panda/patches",
		"classes" => []
	],
	(object) [
		"name"	=> "logout",
		"icon"	=> file_get_contents(base_url() . "assets/icons/close.svg"),
		"link"	=> "javascript:;",
		"classes" => ["logout"]
	]
];
