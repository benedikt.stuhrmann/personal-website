<?php
defined("BASEPATH") || exit("No direct script access allowed");

class DB_Patch {

	/**
	 * @var string Name of the patch.
	 */
	public $name = "";

	/**
	 * This function is called, when the patch gets executed. Put logic in here.
	 * @throws Exception
	 */
	public function patch() {

	} // end patch()

	/**
	 * Loads the database with the specified name.
	 * @param string $database_name Name of the database to load.
	 * @return CI_DB_mysqli_driver The loaded database.
	 */
	public function loadDatabase(string $database_name) : CI_DB_mysqli_driver {
		$CI = get_instance();
		$CI->load->database($database_name);
		$database = $CI->db;
		return $database;
	} // end loadDatabase()

} // end class
