<?php
defined("BASEPATH") || exit("No direct script access allowed");

use DataObjects\UserObj;

/**
 * Class Patch_1574609169
 */
class Patch_1574609169  extends DB_Patch {

	/**
	 * @var string Name of the patch
	 */
	public $name = "Example patch";

	public function patch() {
		// load the required database
		$db = parent::loadDatabase(DB_BESTUH);
		// do your stuff
		$users = $db->get("user")->result(UserObj::class);
	} // end patch()

} // end class
