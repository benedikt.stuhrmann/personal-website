<?php
defined("BASEPATH") || exit("No direct script access allowed");

use DataObjects\UserObj;

/**
 * Class Patch_1575809352
 */
class Patch_1575809352  extends DB_Patch {

	/**
	 * @var string Name of the patch
	 */
	public $name = "Add translations";

	public function patch() {
		// load the required database
		$db = parent::loadDatabase(DB_BESTUH);

		$CI = get_instance();
		$CI->load->library("translator");

		$CI->translator->addText("btn_login", "Login", "Login", ["panda/pages/login"]);
		$CI->translator->addText("placeholder_username", "Enter username", "Username eingeben", ["panda/pages/login"]);
		$CI->translator->addText("placeholder_password", "Enter password", "Passwort eingeben", ["panda/pages/login"]);
		$CI->translator->addText("message_wrong_login", "Wrong username or password", "Username oder Passwort falsch", ["panda/pages/login"]);
		$CI->translator->addText("message_login_error",
			"Error while trying to log in. Try again later.",
			"Fehler beim Login. Bitte versuche es später erneut.",
			["panda/pages/login"]
		);
		$CI->translator->addText("parsley_password_required", "Please enter your password", "Bitte gib dein Passwort ein", ["panda/pages/login"]);
		$CI->translator->addText("parsley_username_required", "Please enter your username", "Bitte gib deinen Username ein", ["panda/pages/login"]);

		$CI->translator->addText("page_title_dashboard",
			"Dashboard",
			"Dashboard",
			["panda/pages/dashboard"]
		);
		$CI->translator->addText("page_subtitle_dashboard",
			"Some fancy graphs and stuff.",
			"Ein paar coole Graphen und sowas.",
			["panda/pages/dashboard"]
		);
		$CI->translator->addText("window_title_dashboard",
			"PANDA - Dashboard",
			"PANDA - Dashboard",
			["panda/pages/dashboard"]
		);

		$CI->translator->addText("page_title_statistics",
			"Statistics",
			"Statistiken",
			["panda/pages/statistics"]
		);
		$CI->translator->addText("page_subtitle_statistics",
			"View statistics for usage and APIs.",
			"Statistiken über Seiten-Nutzung und APIs.",
			["panda/pages/statistics"]
		);
		$CI->translator->addText("window_title_statistics",
			"PANDA - Statistics",
			"PANDA - Statistiken",
			["panda/pages/statistics"]
		);

		$CI->translator->addText("page_title_projects",
			"Projects",
			"Projekte",
			["panda/pages/projects"]
		);
		$CI->translator->addText("page_subtitle_projects",
			"Create, edit and manage projects on the website.",
			"Erstelle, editiere und manage Projekte auf der Webseite.",
			["panda/pages/projects"]
		);
		$CI->translator->addText("window_title_projects",
			"PANDA - Projects",
			"PANDA - Projekte",
			["panda/pages/projects"]
		);

		$CI->translator->addText("page_title_texts",
			"Texts",
			"Texte",
			["panda/pages/texts"]
		);
		$CI->translator->addText("page_subtitle_texts",
			"Add, edit and manage texts and translations.",
			"Erstelle, editiere und manage Texte und Übersetzungen.",
			["panda/pages/texts"]
		);
		$CI->translator->addText("window_title_texts",
			"PANDA - Texts",
			"PANDA - Texte",
			["panda/pages/texts"]
		);

		$CI->translator->addText("page_title_patches",
			"Database Patches",
			"Datenbank Patches",
			["panda/pages/patches"]
		);
		$CI->translator->addText("page_subtitle_patches",
			"Run new database patches, as well as view and manage old ones.",
			"Führe neue Datenbank Patches aus und verwalte bereits ausgeführte.",
			["panda/pages/patches"]
		);
		$CI->translator->addText("window_title_patches",
			"PANDA - DB Patches",
			"PANDA - DB Patches",
			["panda/pages/patches"]
		);

		$CI->translator->addText("btn_execute_patches",
			"Execute",
			"Ausführen",
			["panda/pages/patches"]
		);

		$CI->translator->addText("heading_patches_archive",
			"Archive",
			"Archiv",
			["panda/pages/patches"]
		);

		$CI->translator->addText("placeholder_search_patches",
			"Search patches...",
			"Patches durchsuchen...",
			["panda/pages/patches"]
		);

		$CI->translator->addText("btn_load_more",
			"Load more results",
			"Mehr Ergebnisse laden",
			["panda/pages/patches"]
		);

		$CI->translator->addText("patch_status_archived",
			"Archived",
			"Archiviert",
			["panda/pages/patches"]
		);

		$CI->translator->addText("patch_status_new",
			"New",
			"Neu",
			["panda/pages/patches"]
		);

		$CI->translator->addText("patch_status_error",
			"Error",
			"Error",
			["panda/pages/patches"]
		);

		$CI->translator->addText("patch_status_executed",
			"Executed",
			"Ausgeführt",
			["panda/pages/patches"]
		);

		$CI->translator->addText("patch_status_waiting",
			"Waiting",
			"Warte",
			["panda/pages/patches"]
		);

		$CI->translator->addText("patch_status_running",
			"Running",
			"Läuft",
			["panda/pages/patches"]
		);

		$CI->translator->addText("patch_status_success",
			"Success",
			"Erfolg",
			["panda/pages/patches"]
		);

		$CI->translator->addText("patch_status_canceled",
			"Canceled",
			"Abgebrochen",
			["panda/pages/patches"]
		);

		$CI->translator->addText("patch_click_to_execute",
			"Click above to execute",
			"Zum Ausführen oben klicken",
			["panda/pages/patches"]
		);

		$CI->translator->addText("patch_executed",
			"Executed:",
			"Ausgeführt:",
			["panda/pages/patches"]
		);

		$CI->translator->addText("logged_in_prefix",
			"Logged in as",
			"Eingeloggt als",
			[
				"panda/pages/dashboard",
				"panda/pages/statistics",
				"panda/pages/projects",
				"panda/pages/texts",
				"panda/pages/patches",
			]
		);

		$CI->translator->addText("nav_label_dashboard",
			"Dashboard",
			"Dashboard",
			[
				"panda/pages/dashboard",
				"panda/pages/statistics",
				"panda/pages/projects",
				"panda/pages/texts",
				"panda/pages/patches",
			]
		);

		$CI->translator->addText("nav_label_statistics",
			"Statistics",
			"Statistiken",
			[
				"panda/pages/dashboard",
				"panda/pages/statistics",
				"panda/pages/projects",
				"panda/pages/texts",
				"panda/pages/patches",
			]
		);

		$CI->translator->addText("nav_label_projects",
			"Projects",
			"Projekte",
			[
				"panda/pages/dashboard",
				"panda/pages/statistics",
				"panda/pages/projects",
				"panda/pages/texts",
				"panda/pages/patches",
			]
		);

		$CI->translator->addText("nav_label_texts",
			"Texts",
			"Texte",
			[
				"panda/pages/dashboard",
				"panda/pages/statistics",
				"panda/pages/projects",
				"panda/pages/texts",
				"panda/pages/patches",
			]
		);

		$CI->translator->addText("nav_label_patches",
			"Patches",
			"Patches",
			[
				"panda/pages/dashboard",
				"panda/pages/statistics",
				"panda/pages/projects",
				"panda/pages/texts",
				"panda/pages/patches",
			]
		);

		$CI->translator->addText("nav_label_logout",
			"Logout",
			"Ausloggen",
			[
				"panda/pages/dashboard",
				"panda/pages/statistics",
				"panda/pages/projects",
				"panda/pages/texts",
				"panda/pages/patches",
			]
		);

	} // end patch()

} // end class
