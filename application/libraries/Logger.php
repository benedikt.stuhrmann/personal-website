<?php
defined("BASEPATH") || exit("No direct script access allowed");

/**
 * Class Logger
 */
class Logger {

	/**
	 * @var CI_Controller Codeigniter instance.
	 */
  private $CI;

  /**
   * Logger constructor.
   */
  public function __construct() {
    $this->CI = get_instance();
  } // end __construct()

	/**
	 * Writes a log message into the logs.
	 * @param string $message	The log message.
	 */
	public function logMessage(string $message) {
		log_message("info", $message);
	} // end logMessage()

	/**
	 * Writes an error message into the logs.
	 * @param string $error_msg		The error message.
	 * @param bool   $show_error	If the error should be shown (defaults to true in dev-mode, otherwise false).
	 */
	public function logError(string $error_msg, bool $show_error = SHOW_ERRORS) {
		$error_heading = "Error in " . __FILE__ . " " . __LINE__ . " in function " . __CLASS__ . "->" . __FUNCTION__ . "()";
		log_message("error", "$error_heading: $error_msg");
		if ($show_error) {
			show_error($error_msg, 500, $error_heading);
		}
	} // end logError()

	/**
	 * Writes an error message into the logs.
	 * @param string $error_msg		The error message.
	 * @param bool   $show_error	If the error should be shown (defaults to true in dev-mode, otherwise false).
	 */
	public function logWarning(string $error_msg, bool $show_error = SHOW_WARNINGS) {
		$error_heading = "Warning in " . __FILE__ . " " . __LINE__ . " in function " . __CLASS__ . "->" . __FUNCTION__ . "()";
		log_message("error", "$error_heading: $error_msg");
		if ($show_error) {
			show_error($error_msg, 500, $error_heading);
		}
	} // end logWarning()

} // end class
