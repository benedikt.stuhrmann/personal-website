<?php
defined("BASEPATH") || exit("No direct script access allowed");

/**
 * Class Session.
 * For custom session handling.
 */
class Session {

	/**
	 * @var CI_Controller Codeigniter instance.
	 */
  private $CI;

  /**
   * Session constructor.
   */
  public function __construct() {
    $this->CI = get_instance();
    $this->start();
  } // end __construct()

	/**
	 * Stars a new or existing session.
	 * @param bool $force_new By default an existing session will be resumed (if exists).
	 *                        When this is set to false, a new session will be started instead of resuming the existing one.
	 */
	public function start(bool $force_new = false) {
		if ($this->active() && !$force_new) {
			// if session is already active, do nothing
			return;
		} else if ($this->active()) {
			// if session if already active and start is forces, destroy the current session first
			$this->stop();
		}

		if (!$force_new) {
			// set session id if exists, to start an existing session
			$session_id = $this->sessionId();
			if ($session_id) {
				session_id($session_id);
			}
		}

		session_start();
	} // end start();

	/**
	 * Stops the current session.
	 */
	public function stop() {
		if ($this->active()) {
			$session_id = $this->sessionId();
			if ($session_id) {
				session_id($session_id);
			}
			session_destroy();
		}
	} // end stop();

	/**
	 * Checks if a session is currently running.
	 * @return bool True, if session is running, otherwise false.
	 */
	public function active() : bool {
		$session_status = session_status();
		return ($session_status === PHP_SESSION_ACTIVE);
	} // end active()

	/**
	 * Retrieves the session id from the users session cookie.
	 * @return string|null The session id.
	 */
	public function sessionId() {
		$session_id = get($_COOKIE, "PHPSESSID");
		return $session_id;
	} // end sessionId()

	/**
	 * Sets or reads data inside the session.
	 * @param string $key 	  Key of the data attribute in the session.
	 * @param string $value 	Value for the data attribute.
	 *                      	When set to "get" (default), session data will be read instead of set.
	 * @return mixed|string		When $value is set to "get", the value of the specified attribute will be returned.
	 */
	public function data(string $key, $value = "get") {
  	if ($value === "get") {
			$value = get($_SESSION, $key);
			return $value;
		} else {
			$_SESSION[$key] = $value;
		}
	} // end data()

	/**
	 * Unsets the data attribute with the given $key in the current session.
	 * @param string $key Key of the data attribute in the session.
	 */
	public function unsetData(string $key) {
		if (array_key_exists($key, $_SESSION)) {
			unset($_SESSION[$key]);
		}
	} // end unsetData()

	/**
	 * Clears/Unsets all session data.
	 */
	public function clear() {
		if ($this->active()) {
			foreach ($_SESSION as $session_item_name => $session_item_value) {
				$this->unsetData($session_item_name);
			}
		}
	} // end clear()

} // end class
