<?php
defined('BASEPATH') || exit('No direct script access allowed');

use DataObjects\LanguageObj;
use DataObjects\ErrorObj;

class Translator {

  private $CI;
  private $lang;
  private $texts;

  /**
   * Translator constructor.
   * @param array $params (optional) Possible params are:
   *                      Key: "view" Value: name of the view => All texts used in this view are loaded automatically.
   */
  public function __construct(array $params = []) {
    $this->CI = get_instance();
    $this->CI->load->model("text_model");

    $this->lang = $this->getUserLanguage();

    // load texts for view, if one is specified
    if (isset($params["view"])) $this->loadTextsForView($params["view"]);
  } // end __construct()

  /**
   * Gets the currently set language of the translator.
   * @return string Language code of the currently set language.
   */
  public function getLanguage() {
    return $this->lang;
  } // end getLanguage()

  /**
   * Sets the language.
   * If the user selected a language on the site, that language will be taken.
   * Otherwise determines the users browser language and returns that.
   * @return string Language code of the determined language
   */
  private function getUserLanguage() {
    // fallback to english
    $language = "en";

    // if user selected a language take that
    $query_params = getQueryParams();
    if (array_key_exists("lang", $query_params)) {
      $language = $query_params["lang"];
      return $language;
    }

    // otherwise use users browser language
    $browser_languages = explode(",", $_SERVER["HTTP_ACCEPT_LANGUAGE"]);
    $supported_languages = $this->CI->text_model->getLanguageCodes();

    // select first browser language supported by the application
    foreach ($browser_languages as $browser_language) {
      $browser_language = substr($browser_language, 0, 2);
      if (in_array($browser_language, $supported_languages)) {
        $language = $browser_language;
        break;
      }
    }

    return $language;
  } // end getUserLanguage()

  /**
   * Gets the text associated with the specified code.
   * Automatically retrieves the correct translation for the text element, depending on the the users system language.
   * @param string $code     Code of a text_element. Text elements are stored in the database in the text_elements table.
   * @param string $fallback String that will be returned if the specified code does not exist (default: "")
   * @return string The translated text associated with the specified code.
   */
  public function get(string $code, string $fallback = "") {
    if (isset($this->texts) && array_key_exists($code, $this->texts)) {
      return $this->texts[$code];
    } else {
      // if does not exist in $texts, search for it in the database
      $text = $this->CI->text_model->get($code, $this->lang);
      if ($text === false) {
        // if not found in database, the text element does not exist
				$this->CI->logger->logError("Did not found any text element with code '$code'.", false);
      } else {
        return $text;
      }
      return $fallback;
    }
  } // end get()

  /**
   * Loads all required texts for the specified view into the $texts array.
   * @param string $view  The name of the view.
   * @param bool   $reset If set to true (default: false) the $tests array will be emptied before loading the texts.
   *                      Otherwise (by default) newly loaded texts will be added to the current set of texts.
   */
  public function loadTextsForView(string $view, bool $reset = false) {
    $texts = $this->CI->text_model->textsForView($view, $this->lang);
    if (sizeof($texts)) {
      if ($reset) $this->texts = [];
      $this->texts = $texts;
    }
  } // end loadTextsForView()

	/**
	 * Creates a new text.
	 * @param string 		$code 						Unique code of the text. Used for referencing it in templates.
	 * @param string 		$english_text			English version of the text.
	 * @param string 		$german_text			German version of the text.
	 * @param String[] 	$template_codes		An array of template codes, to specify in what views the text can be displayed.
	 * @return null|ErrorObj
	 */
	public function addText(string $code, string $english_text, string $german_text, array $template_codes) {
		$text_element = $this->CI->text_model->insert_text($code);
		if ($text_element instanceof ErrorObj) {
			return $text_element;
		}

		$this->CI->text_model->insert_translation($text_element->id, LanguageObj::LANG_EN, $english_text);
		$this->CI->text_model->insert_translation($text_element->id, LanguageObj::LANG_GER, $german_text);
		foreach ($template_codes as $template_code) {
			$this->CI->text_model->insert_template_text($text_element->id, $template_code);
		}
	} // end addText()

} // end class
