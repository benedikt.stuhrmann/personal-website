<?php
defined('BASEPATH') || exit('No direct script access allowed');

use DataObjects\UserObj;

/**
 * Class Users
 * User manager to handle everything around users.
 * @property-read Authenticator authenticator
 */
class Users {

	/**
	 * @var CI_Controller Codeigniter instance.
	 */
	private $CI;

	/**
	 * Users constructor.
	 */
	public function __construct() {
		$this->CI = get_instance();
	} // end __construct()

	/**
	 * Gets all users matching the specified filter.
	 * @param array $filter Associative filter array.
	 * @param bool $force_array If set to true (default: false), instead of returning null or a single UserObj,
	 *                          the return type will always be an array.
	 * @return UserObj[]|UserObj|null Array of user objects. If only one matching user exists a single UserObj.
	 *                                If no matches, NULL.
	 */
	public function get(array $filter, bool $force_array = false) {
		$users = $this->CI->user_model->get($filter);
		if (empty($users)) {
			return null;
		} else if (sizeof($users) === 1) {
			$single_user = array_shift($users);
			return $single_user;
		} else {
			return $users;
		}
	} // end get()

	/**
	 * Login the user.
	 * @param string $username The username entered by the user.
	 * @param string $password The password (unhashed) entered by the user.
	 * @return bool True, if successfully logged in. False, if incorrect username or password.
	 */
	public function login(string $username, string $password) : bool {
		$logged_in = $this->CI->authenticator->login($username, $password);
		return $logged_in;
	} // end login()

	/**
	 * Logs out the user in the current session.
	 * @return bool True on successful logout, otherwise false.
	 */
	public function logout() {
		$logged_out = $this->CI->authenticator->logout();
		return $logged_out;
	} // end logout()

} // end class
