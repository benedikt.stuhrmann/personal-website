<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Class Authentication
 * Manages everything around user authentication like login and logout.
 * @property-read Users users
 * @property-read Session session
 */
class Authenticator {

	/**
	 * @var CI_Controller Codeigniter instance.
	 */
	private $CI;

	/**
	 * Authentication constructor.
	 */
	public function __construct() {
		$this->CI = get_instance();
	} // end __construct()

	/**
	 * Login the user.
	 * @param string $username The username entered by the user.
	 * @param string $password The password (unhashed) entered by the user.
	 * @return bool True, if successfully logged in. False, if incorrect username or password.
	 */
	public function login(string $username, string $password) : bool {
		$user = $this->CI->users->get(["username" => $username]);
		if (!$user) {
			// no user with that username exists
			$this->CI->logger->logMessage("Failed login attempt due to unknown username '$username'.");
			return false;
		}

		if (!$this->check_password($password, $user->getPassword())) {
			// user exists but password is incorrect
			$this->CI->logger->logMessage("Failed login attempt due to incorrect password '$password' for user with username '$username'.");
			return false;
		}

		$this->CI->session->data("logged_in", true);
		$this->CI->session->data("user", $user);

		return true;
	} // end login()

	/**
	 * Logs out the user in the current session.
	 * @return bool True on successful logout, otherwise false.
	 */
	public function logout() : bool {
		$this->CI->session->clear();
		$logged_out = !$this->loggedIn();
		return $logged_out;
	} // end logout()

	/**
	 * Checks if the user is logged in.
	 * @return bool True if logged in, otherwise false.
	 */
	public function loggedIn() {
		$logged_in = $this->CI->session->data("logged_in");
		return ($logged_in === true);
	} // end loggedIn()

	/**
	 * Hashes a passed password.
	 * @param string $password Password in clear text.
	 * @return string The hashed password.
	 */
	private function hash_password(string $password) : string {
		$hashed_password = password_hash($password, PASSWORD_HASH_ALGORITHM);
		return $hashed_password;
	} // end hash_password()

	/**
	 * Checks if the password in clear text matches the hashed one.
	 * @param string $password_clear		Password in clear text (unhashed).
	 * @param string $password_hashed		Hashed password.
	 * @return bool True, if passwords match, otherwise false.
	 */
	private function check_password(string $password_clear, string $password_hashed) : bool {
		$matches = password_verify($password_clear, $password_hashed);
		return $matches;
	} // end check_password()

	/**
	 * Checks if the user has access to the site he is requesting.
	 * User will be redirected to login page, if not logged in.
	 * User will be redirected to dashboard if logged in and trying to access login page.
	 */
	public function checkAccess() {
		$login_page = (strpos($_SERVER["REQUEST_URI"], "login") !== false);
		if (!$this->loggedIn() && !$login_page) {
			// if not logged in and not already on the login page => redirect to the login page
			redirect("panda/login");
		} else if ($this->loggedIn() && $login_page && $this->CI->input->method(TRUE) !== "POST") {
			// if logged in and on the login page (and not a post request) => redirect to the dashboard page
			redirect("panda/dashboard");
		}
	} // end checkAccess()

	/**
	 * Checks if the current request is not an ajax request.
	 * If so, a 404 will be shown or a redirect to the specified url will be done.
	 * @param string|null $redirect_url URL to redirect to, if the request is not an ajax request.
	 *                                  If null (default), a 404 will be shown.
	 */
	public function ifOtherThanAjax(string $redirect_url = null) {
		if ($this->CI->input->method(TRUE) !== "POST") {
			if ($redirect_url) {
				redirect($redirect_url);
			} else {
				show_404();
			}
		}
	} // end ifOtherThanAjax()

} // end class
