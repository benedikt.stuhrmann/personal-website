<?php
defined("BASEPATH") || exit("No direct script access allowed");

use DataObjects\PatchObj;

/**
 * Class Patcher
 */
class Patcher {

	/**
	 * @var CI_Controller Codeigniter instance.
	 */
  private $CI;

  /**
   * Patcher constructor.
   */
  public function __construct() {
    $this->CI = get_instance();
  } // end __construct()

	/**
	 * Checks for new patches in the PATCHES_DIR and inserts them into the database.
	 */
	public function checkForPatches() {
		require_once PATCHES_DIR . "DB_Patch.php";
		// get files from patches directory
		$patch_files = scandir(PATCHES_DIR);
		if ($patch_files) {
			// check each file
			foreach ($patch_files as $patch_file) {
				$timestamp = explode(".", $patch_file)[0];
				// check if filename contains timestamp (otherwise skip it)
				if (is_numeric($timestamp)) {
					// load the patch to read out its name
					$classname = "Patch_$timestamp";
					require_once PATCHES_DIR . $patch_file;
					$patch_instance = new $classname();
					// create a new patch
					$patch = new PatchObj([
						"timestamp" => intval($timestamp),
						"name"			=> $patch_instance->name
					]);
					$this->createPatch($patch);
				}
			}
		}
	} // end checkForPatches()

	/**
	 * @param int $timestamp
	 * @throws Exception
	 */
	public function executePatch(int $timestamp) {
		require_once PATCHES_DIR . "DB_Patch.php";

		$filename = PATCHES_DIR . $timestamp . ".patch.php";
		$classname = "Patch_$timestamp";

		if (!file_exists($filename)) {
			$message = "Unable to load patch file '$filename'. File not found in patches directory.";
			$this->CI->logger->logError($message, false);
			throw new Exception($message);
		}

		require_once $filename;
		$patch_instance = new $classname();
		$patch_instance->patch();

		$this->archive($timestamp);
	} // end executePatch()

	/**
	 * Gets the first patch object matching the specified filter.
	 * @param array $filter	Associative filter array.
	 * @return PatchObj	The patch object;
	 */
	public function getPatch(array $filter) : PatchObj {
		$patch = $this->CI->patch_model->get($filter)[0];
		return $patch;
	} // end getPatch()

	/**
	 * Gets patches from the database
	 * @param string $type 					The type of patches to return. Either PatchObj::EXECUTED, PatchObj::UNEXECUTED or PatchObj::ALL (default).
	 * @param int    $amount				Number of patches to get (default: 99999).
	 * @param int    $offset				Offset in the dataset (default: 0).
	 * @param bool   $newest_first	Order patches, so newest ones are first (default).
	 *                             	When set to false, oldest ones will be first.
	 * @return array Array with the keys "total" (count) and "patches".
	 * 							 When type PatchObj::ALL is specified, an array with the keys "total" (count), "executed" and "unexecuted" will be returned.
	 */
	public function getPatches(string $type = PatchObj::ALL, string $search = null, int $amount = 99999, int $offset = 0, bool $newest_first = true) : array {
		if (!PatchObj::validType($type)) {
			return [];
		}

		$filter = [];
  	if ($type === PatchObj::EXECUTED) {
			$filter = ["executed !=" => null];
		} else if ($type === PatchObj::UNEXECUTED) {
			$filter = ["executed" => null];
		}

  	$patches = $this->CI->patch_model->get($filter, $search, $amount, $offset, $newest_first);
  	$total_number_of_patches = $this->CI->patch_model->getCount($filter);

  	if ($type !== PatchObj::ALL) {
			return [
				"patches" => $patches,
				"total"		=> $total_number_of_patches
			];
		} else {
			$seperated_patches = [
				PatchObj::EXECUTED => [],
				PatchObj::UNEXECUTED => []
			];
			foreach ($patches as $patch) {
				$executed = empty($patch->getExecuted()) ? PatchObj::UNEXECUTED : PatchObj::EXECUTED;
				$seperated_patches[$executed][] = $patch;
			}
			return [
				PatchObj::EXECUTED 	 => $seperated_patches[PatchObj::EXECUTED],
				PatchObj::UNEXECUTED => $seperated_patches[PatchObj::UNEXECUTED],
				"total" => $total_number_of_patches
			];
		}
	} // end getPatches()

	/**
	 * Creates a new patch. Has to have a timestamp set.
	 * @param PatchObj $patch The patch to create.
	 * @return int|bool  Timestamp on success (primary key).
	 * 									 False on failure.
	 */
	public function createPatch(PatchObj $patch) {
  	if (!$patch->getTimestamp()) {
  		return false;
		}
		$timestamp = $this->CI->patch_model->insert($patch);
		return $timestamp;
	} // end createPatch()

	/**
	 * Adds a patch to the archive.
	 * @param int  $timestamp	Timestamp of the patch.
	 * @param bool $reset			Instead of archiving the patch, it will be removed from the archive.
	 * @return bool Success.
	 */
	public function archive(int $timestamp, bool $reset = false) : bool {
		if (!$reset) {
			// add to archive
			try {
				$now = new DateTime();
			} catch (Exception $e) {
				$this->CI->logger->logError("Could not create DateTime object.");
				return false;
			}
			$updated_records = $this->CI->patch_model->update(
				["executed" => $now->format("Y-m-d H:i:s")],
				["timestamp" => $timestamp]
			);
		} else {
			// remove from archive
			$updated_records = $this->CI->patch_model->update(
				["executed" => null],
				["timestamp" => $timestamp]
			);
		}

		return $updated_records > 0;
	} // end archive()

} // end class
