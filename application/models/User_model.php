<?php
defined('BASEPATH') || exit('No direct script access allowed');

use DataObjects\UserObj;

class User_model extends CI_Model {

  /**
   * Text_model.php constructor.
   * Connects to the database.
   */
  public function __construct() {
    parent::__construct();

    $this->load->database(DB_BESTUH);
  } // end __construct();


	/**
	 * Gets all users matching the specified filter.
	 * @param array $filter Associative filter array.
	 * @return UserObj[]
	 */
  public function get(array $filter) {
    $users = $this->db
      ->get_where("user", $filter)
      ->result(UserObj::class);

    return $users;
  } // end get()


} // end class
