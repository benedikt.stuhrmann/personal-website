<?php
defined('BASEPATH') || exit('No direct script access allowed');

use DataObjects\UserObj;
use DataObjects\ErrorObj;

class Text_model extends CI_Model {

  /**
   * Text_model constructor.
   * Connects to the database.
   */
  public function __construct() {
    parent::__construct();

    $this->load->database(DB_BESTUH);
  } // end __construct();

  /**
   * Retrieves languages codes for all languages supported by the application.
   * @return string[] Array of language codes
   */
  public function getLanguageCodes(): array {
    $query = $this->db
      ->select("lang_code")
      ->get("language");

    $lang_codes = array_column($query->result_array(), "lang_code");
    return $lang_codes;
  } // end getLanguageCodes()

  /**
   * Gets the text associated with the specified code in the specified language.
   * @param string $code Text element code.
   * @param string $lang Language code for translation.
   * @return string|bool Text associated with the code in the specified language.
   *                     Or false if no text is found.
   */
  public function get(string $code, string $lang) {
    $sql = "SELECT trn.text 
            FROM translation trn 
              LEFT JOIN text_element txt_elem ON (trn.text_element_id = txt_elem.id)
              LEFT JOIN language lang ON (trn.language_id = lang.id)
            WHERE txt_elem.code = ? 
              AND lang.lang_code = ?";

    $query = $this->db->query($sql, [$code, $lang]);
    $text = $query->row_array()["text"];

    if ($text === null) {
      return false;
    } else {
      return $text;
    }
  } // end get()

  /**
   * Gets all texts for the specified view in the specified language.
   * @param string $view   Name/Code of the view.
   * @param string $lang   Language code for translation.
   * @return string[] Texts for the specified view in the specified language.
   */
  public function textsForView(string $view, string $lang): array {
    $sql = "SELECT 
              txt_elem.code, 
              trn.text 
            FROM translation trn 
              LEFT JOIN template_text_element view ON (trn.text_element_id = view.text_element_id)
              LEFT JOIN text_element txt_elem ON (trn.text_element_id = txt_elem.id)
              LEFT JOIN language lang ON (trn.language_id = lang.id)
            WHERE view.template_code = ? 
              AND lang.lang_code = ?";

    $query = $this->db->query($sql, [$view, $lang]);

    $texts = [];
    foreach($query->result_array() as $index => $text_row) {
      $texts[$text_row["code"]] = $text_row["text"];
    }

    return $texts;
  } // end textsForView()

	/**
	 * Checks if the text element exists.
	 * @param string $code Unique identifier-code of a text element.
	 * @return bool	True, if a text-element with that code exists. Otherwise false.
	 */
	public function text_exists(string $code) : bool {
		$text_element = $this->db->get_where("text_element", ["code" => $code])->result();
		$exists = sizeof($text_element) > 0;
		return $exists;
	} // end text_exists()

	/**
	 * Checks if the translation element exists.
	 * @param int $text_element_id 	Id of the text element.
	 * @param int $language_id 			Language id.
	 * @return bool	True, if a translation for that element in that language exists. Otherwise false.
	 */
	public function translation_exists(int $text_element_id, int $language_id) : bool {
		$translation = $this->db->get_where("translation", [
			"text_element_id" => $text_element_id,
			"language_id" => $language_id
		])->result();
		$exists = sizeof($translation) > 0;
		return $exists;
	} // end translation_exists()

	/**
	 * Checks if the template text exists.
	 * @param int $text_element_id 		Id of the text element.
	 * @param string $template_code 	Code of the template/view.
	 * @return bool	True, if a template text for that element and that view does exists. Otherwise false.
	 */
	public function template_text_exits(int $text_element_id, string $template_code) : bool {
		$template_text = $this->db->get_where("template_text_element", [
			"text_element_id" => $text_element_id,
			"template_code" => $template_code
		])->result();
		$exists = sizeof($template_text) > 0;
		return $exists;
	} // end template_text_exits()

	/**
	 * Gets a text element from the database.
	 * @param int $text_element_id	Id of the text element to get.
	 * @return stdClass The text element.
	 */
	private function get_text_element(int $text_element_id) :stdClass {
		$text_element = $this->db->get_where("text_element", ["id" => $text_element_id])->result();
		return array_shift($text_element);
	} // end get_text_element()

	/**
	 * Gets a translation from the database.
	 * @param int $translation_id	Id of the translation to get.
	 * @return stdClass The translation element.
	 */
	private function get_translation(int $translation_id) :stdClass {
		$translation = $this->db->get_where("translation", ["id" => $translation_id])->result();
		return array_shift($translation);
	} // end get_translation()

	/**
	 * Gets a template text from the database.
	 * @param int $template_text_id 		Id of the template text element.
	 * @return stdClass The template text.
	 */
	private function get_template_text(int $template_text_id) :stdClass {
		$template_text = $this->db->get_where("template_text_element", ["id" => $template_text_id])->result();
		return array_shift($template_text);
	} // end get_template_text()

	/**
	 * Inserts a new text element into the database.
	 * @param string $code	Code of the new text element.
	 * @return stdClass|ErrorObj The created text element. An error if a text element with that code already exists.
	 */
	public function insert_text(string $code) {
		if ($this->text_exists($code)) {
			return new ErrorObj(["message" => "A text with this code does already exist."]);
		}
		$current_user = $this->session->data("user")->getId() ?? UserObj::APP_USER;
		$this->db->insert("text_element", [
			"code" => $code,
			"creator" => $current_user,
			"editor" => $current_user
		]);
		$text_element_id = $this->db->insert_id();
		$text_element = $this->get_text_element($text_element_id);
		return $text_element;
	} // end insert_text()

	/**
	 * Inserts a new translation into the database.
	 * @param int    $text_element_id	Text element id.
	 * @param int    $language_id			Language id.
	 * @param string $text						The text.
	 * @return ErrorObj|stdClass The created translation. An error if a translation in that language for that text already exists.
	 */
	public function insert_translation(int $text_element_id, int $language_id, string $text) {
		if ($this->translation_exists($text_element_id, $language_id)) {
			return new ErrorObj(["message" => "A translation in that language for that text already exists."]);
		}

		$current_user = $this->session->data("user")->getId() ?? UserObj::APP_USER;
		$this->db->insert("translation", [
			"text_element_id" => $text_element_id,
			"language_id" => $language_id,
			"text" => $text,
			"creator" => $current_user,
			"editor" => $current_user
		]);
		$translation_id = $this->db->insert_id();
		$translation = $this->get_translation($translation_id);
		return $translation;
	} // end insert_translation()

	/**
	 * Inserts a new template text into the database.
	 * @param int    $text_element_id	Text element id.
	 * @param string $template_code		Code of the template/view.
	 * @return ErrorObj|stdClass The created template text. An error if the text element is already assigned to that template.
	 */
	public function insert_template_text(int $text_element_id, string $template_code) {
		if ($this->template_text_exits($text_element_id, $template_code)) {
			return new ErrorObj(["message" => "This text element is already assigned to that template."]);
		}

		$current_user = $this->session->data("user")->getId() ?? UserObj::APP_USER;
		$this->db->insert("template_text_element", [
			"text_element_id" => $text_element_id,
			"template_code" => $template_code,
			"creator" => $current_user,
			"editor" => $current_user
		]);
		$template_text_id = $this->db->insert_id();
		$template_text = $this->get_template_text($template_text_id);
		return $template_text;
	} // end insert_template_text()

} // end class
