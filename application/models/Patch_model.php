<?php
defined('BASEPATH') || exit('No direct script access allowed');

use DataObjects\PatchObj;

class Patch_model extends CI_Model {

  /**
   * Patch_model.php constructor.
   * Connects to the database.
   */
  public function __construct() {
    parent::__construct();

    $this->load->database(DB_BESTUH);
  } // end __construct();

	/**
	 * Get all columns from patch table.
	 * @return array Array of columns
	 */
	private function getColumns() : array {
  	$columns = $this->db->list_fields("patch");
  	return $columns;
	} // end getColumns()

	/**
	 * Gets all patches matching the specified filter.
	 * @param array $filter					Associative filter array.
	 * @param string $search				Search string. Null (default) for no search.
	 * @param int   $limit					Number of patches to get.
	 * @param int   $offset					Offset in the dataset.
	 * @param bool  $newest_first		Order patches, so newest ones are first (default).
	 *                             	When set to false, oldest ones will be first.
	 * @return PatchObj[] Array of patch objects.
	 */
  public function get(array $filter, string $search = null, int $limit = null, int $offset = 0, bool $newest_first = true) {
		$this->db->where($filter, NULL, FALSE);

		if ($search) {
			$search_words = explode(" ", $search);
			foreach ($search_words as $search_word) {
				$this->db->group_start();
				foreach ($this->getColumns() as $index => $column) {
					if ($index === 0) {
						$this->db->like($column, $search_word);
					} else {
						$this->db->or_like($column, $search_word);
					}
				}
				$this->db->group_end();
			}
		}

		if ($newest_first) {
			$this->db->order_by("timestamp", "DESC");
		} else {
			$this->db->order_by("timestamp", "ASC");
		}

		if ($limit >= 0 && $offset >= 0) {
			$this->db->limit($limit, $offset);
		} else if ($limit >= 0) {
			$this->db->limit($limit, 0);
		}

		$patches = $this->db->get("patch")->result(PatchObj::class);
    return $patches;
  } // end get()

	/**
	 * Gets the number of patches matching the specified filter.
	 * @param array $filter	Associative filter array.
	 * @return int Number of matching patches.
	 */
	public function getCount(array $filter) : int {
		$count = $this->db
			->from("patch")
			->where($filter, NULL, FALSE)
			->count_all_results();
		return $count;
	} // end getCount()

	/**
	 * Inserts a patch into the database.
	 * @param PatchObj $patch 				Patch object to be inserted.
	 * @param bool     $get_inserted	If set to true (default: false), the inserted patch will be returned, instead of the timestamp.
	 * @return bool|PatchObj[]|int    Timestamp of the created patch on success.
	 *                                When $get_inserted is set to true, the inserted patch object.
	 * 																False on failure.
	 */
	public function insert(PatchObj $patch, bool $get_inserted = false) {
  	$insert_data = $patch->getInsertData();
		$insert_string = $this->db->insert_string("patch", $insert_data) . " ON DUPLICATE KEY UPDATE timestamp = timestamp";
		$success = $this->db->query($insert_string);
		if ($success) {
			$insert_id = $patch->getTimestamp();
			if (!$get_inserted) {
				return $insert_id;
			} else {
				$inserted_patch = $this->get(["timestamp" => $insert_id]);
				return $inserted_patch;
			}
		} else {
			return false;
		}
	} // end insert()

	/**
	 * Updates a patch in the database.
	 * @param array $update_data Associative array of data to update.
	 * @param array $where			 Associative array for where clause.
	 * @return int Updated records.
	 */
	public function update(array $update_data, array $where) : int {
		$updated_records = $this->db->update("patch", $update_data, $where);
		return $updated_records;
	} // end update()

} // end class
