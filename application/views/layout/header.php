<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<header class="out <?= $anchor ?>">
  <div id="badge">
    <div class="badge-inner locked">
      <a id="logo" href="<?= base_url() ?>">BS</a>
      <?php if ($device === "desktop") { ?>
      <div id="panda">
        <a class="panda" href="<?= base_url() ?>panda/login">
          <img src="<?= base_url() ?>assets/icons/panda.svg" alt="Panda">
        </a>
      </div>
      <?php } ?>
    </div>
  </div>
  <?php if ($device === "mobile") { ?>
  <div id="hamburger-menu">
    <div id="one"></div>
    <div id="two"></div>
    <div id="three"></div>
  </div>
  <?php } ?>
  <?php $this->load->view("layout/nav") ?>
</header>
