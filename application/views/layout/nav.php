<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<nav class="<?= $device ?>">
  <ul>
    <li class="active"><a href="#home" data-target="home" class="scroll-nav"><?= $text->get("nav_home") ?></a></li>
    <!-- /* <li><a href="#projects" data-target="projects" class="scroll-nav"><?= $text->get("nav_projects") ?></a></li> */ -->
    <li><a href="#contact" data-target="contact" class="scroll-nav"><?= $text->get("nav_contact") ?></a></li>
    <li class="underline"></li>
    <li>
      <span class="vr"></span>
      <a href="?lang=<?= ($text->getLanguage() === "de") ? "en" : "de" ?>">
        <?php if ($device !== "mobile") { ?>
        <div id="lang-list">
          <span><?= ($text->getLanguage() === "de") ? $text->get("label_de") : $text->get("label_en") ?></span>
          <span><?= ($text->getLanguage() === "de") ? $text->get("label_en") : $text->get("label_de") ?></span>
        </div>
        <?php } else { ?>
        <div id="lang-mobile">
          <span><?= ($text->getLanguage() === "de") ? $text->get("label_en") : $text->get("label_de") ?></span>
        </div>
        <?php } ?>
      </a>
    </li>
  </ul>
</nav>
