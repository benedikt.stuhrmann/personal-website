<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<div class="project-card">
	<div class="header <?= $header_color ?>">
		<h3 class="title"><?= $title ?></h3>
		<?php if (isset($subtitle)) : ?>
			<h4 class="subtitle"><?= $subtitle ?></h4>
		<?php endif; ?>
		<p class="blurb"><?= $blurb ?><span class="show-more"><i>show more</i></span></p>
		<div class="actions">
			<a href="" class="btn view-code">
				<span class="btn-text">View Code</span>
				<span class="logo-sm btn-logo"><img src="<?= base_url() ?>assets/icons/logo-gitlab-midnight.svg" alt="GitLab Logo"/></span>
			</a>
			<a href="" class="btn view-project">
				<span class="logo-sm btn-logo"><img src="<?= base_url() ?>assets/icons/play.svg" alt="GitLab Logo"/></span>
			</a>
		</div>
	</div>
	<div class="content">
		<div class="hero"><img src="<?= base_url() ?>assets/images/blumen-frensch.jpg"/></div>
		<div class="details"></div>
	</div>
</div>
