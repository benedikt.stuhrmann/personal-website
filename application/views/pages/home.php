<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<section id="home">
  <div id="mesh">
	<canvas id="mesh-anim"></canvas>
</div>
<div id="welcome">
	<h1><?= $text->get("welcome_text") ?></h1>
	<div class="hr out"></div>
	<p id="me-short" class="out">
		<i><?= $text->get("me_short") ?></i>
	</p>
</div>
<a class="page-down out loop scroll-nav" href="javascript:;" data-target="contact">
	<img src="<?= base_url() ?>assets/icons/caret_down.svg" alt="<?= $text->get("alt_caret_down") ?>">
</a>
</section>
