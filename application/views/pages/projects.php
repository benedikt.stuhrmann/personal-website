<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<section id="projects">
  <div class="section-content">
    <h2 class="section-title"><?= $text->get("projects_heading") ?></h2>
    <ol class="projects">
			<li class="project">
				<?php $this->load->view("layout/project_card", [
					"title" => "Blumen Frensch",
					"blurb" => "Complete redesign and implementation</br>for a floristic shop.",
					"header_color" => "bloodburst"
				]) ?>
			</li>
			<li class="project">
				<?php $this->load->view("layout/project_card", [
					"title" => "SOLID Todolist",
					"blurb" => "Lightweight todolist web application built on<br/>the Solid Specification.",
					"header_color" => "deep-midnight"
				]) ?>
			</li>
			<li class="project">
				<?php $this->load->view("layout/project_card", [
					"title" => "Arcade65",
					"subtitle" => "Overload",
					"blurb" => "Short paced two player game for a 65\" touch screen<br/>developed in Unity3D.",
					"header_color" => "aqua-vitale"
				]) ?>
			</li>
			<li class="project">
				<?php $this->load->view("layout/project_card", [
					"title" => "JS-Game",
					"subtitle" => "Gamification",
					"blurb" => "Web-based 2D dungeon game written completely in<br/>Javascript using PhaserJS.",
					"header_color" => "bloodburst"
				]) ?>
			</li>
		</ol>
  </div>
</section>
