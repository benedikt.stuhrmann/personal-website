<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<section id="contact">
  <div class="section-content">
    <h2 class="section-title"><?= $text->get("contact_heading") ?></h2>
    <div class="mail-container">
      <a href="mailto:mail@benedikt-stuhrmann.de">mail@benedikt-stuhrmann.de</a>
      <div class="underline"></div>
    </div>

    <div class="logo-container">
      <a class="logo-sm logo-hover logo-gitlab" href="https://gitlab.com/benedikt.stuhrmann" title="GitLab" target="_blank"><img src="<?= base_url() ?>assets/icons/logo-gitlab.svg" alt="GitLab Logo"/></a>
      <a class="logo-sm logo-hover logo-linkedin" href="https://www.linkedin.com/in/benedikt-stuhrmann/" title="LinkedIn" target="_blank"><img src="<?= base_url() ?>assets/icons/logo-linkedin.png" alt="LinkedIn Logo"/></a>
      <a class="logo-sm logo-hover logo-xing" href="https://www.xing.com/profile/Benedikt_Stuhrmann2" title="Xing" target="_blank"><img src="<?= base_url() ?>assets/icons/logo-xing.svg" alt="Xing Logo"/></a>
    </div>
  </div>
</section>
