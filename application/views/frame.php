<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<!DOCTYPE html>
<html lang="<?= $text->getLanguage() ?> ">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title ?></title>

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/main.css">

		<script src="<?= base_url() ?>assets/js/libraries/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/libraries/jquery.ba-throttle-debounce.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/typing_animation.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/meshpoint_animation.js" type="text/javascript" defer></script>
		<script src="<?= base_url() ?>assets/js/mobile_menu.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/main.js" type="text/javascript"></script>
  </head>
  <body>
    <script>
      let anchor = "<?= $anchor ?>";
      let lang = "<?= $text->getLanguage() ?>";
      let device = "<?= $device ?>";
    </script>
    <?php $this->load->view("pages/$view") ?>
  </body>
</html>
