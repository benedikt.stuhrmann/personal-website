<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/panda/modals.css">
<script src="<?= base_url() ?>assets/js/panda/modals.js" type="text/javascript"></script>

<div class="modal-container hidden">
	<div class="modal" id="<?= $code ?>-modal">
		<div class="modal-header">
			<h2 class="modal-title"><?= $title ?></h2>
			<span class="close-modal"><?= file_get_contents(base_url() . "assets/icons/close.svg") ?></span>
		</div>
		<div class="modal-body">
			<?= $body ?>
		</div>
		<div class="modal-footer">
			<div class="pull-right">
				<?= $footer ?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
