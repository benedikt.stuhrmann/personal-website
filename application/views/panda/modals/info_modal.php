<?php
defined("BASEPATH") OR exit("No direct script access allowed");

$info_modal_title = "";
$info_modal_body = "<p class='info-text'></p>";
$info_modal_footer = "<button type='button' class='btn close-modal'></button>";
?>

<?php $this->load->view("panda/modals/modal_frame", [
	"code" 		=> "info",
	"title" 	=> $info_modal_title,
	"body"		=> $info_modal_body,
	"footer" 	=> $info_modal_footer
]) ?>
