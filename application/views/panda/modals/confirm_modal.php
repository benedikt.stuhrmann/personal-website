<?php
defined("BASEPATH") OR exit("No direct script access allowed");

$confirm_modal_title = "";
$confirm_modal_body = "<p class='info-text'></p>";
$confirm_modal_footer = "<button type='button' class='btn close-modal'></button><button type='button' class='btn btn-primary confirm-modal'></button>";
?>

<?php $this->load->view("panda/modals/modal_frame", [
	"code" 		=> "confirm",
	"title" 	=> $confirm_modal_title,
	"body"		=> $confirm_modal_body,
	"footer" 	=> $confirm_modal_footer
]) ?>
