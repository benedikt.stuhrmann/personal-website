<?php
defined("BASEPATH") OR exit("No direct script access allowed");
$lang_part = ($text->getLanguage() === "de") ? "" : "?lang=en";
?>

<?php
$open = "";
if ($device !== "mobile") {
	$open = "menu-open";
} ?>
<nav class="<?= $device ?> <?= $open ?>">
	<div class="panda">
		<a href="<?= base_url() . "panda/dashboard" ?>"><?= file_get_contents(base_url() . "assets/icons/panda.svg") ?></a>
	</div>
	<hr/>
	<ul>
		<?php foreach ($nav_items as $nav_item) : ?>
			<?php
			$active = "";
			$request_uri = get($_SERVER, "REQUEST_URI");
			if ($request_uri && strpos($request_uri, $nav_item->name) !== false) {
				$active = "active";
			} ?>
		<li class="<?= $active ?>">
			<a class="<?= implode(" ", $nav_item->classes) ?>" href="<?= $nav_item->link ?><?= $lang_part ?>">
				<span class="icon"><?= $nav_item->icon ?></span>
				<span class="label"><?= $text->get("nav_label_" . $nav_item->name) ?></span>
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
	<div class="logged-in-user">
		<?= $text->get("logged_in_prefix") ?> <strong><?= $this->session->data("user")->getUsername() ?></strong>
	</div>
</nav>
