<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/panda/loader_square_flipper.css">

<div class="loader-square-flipper">
	<div class="square-flip">
		<div class="filled"></div>
	</div>
	<div class="square-flip">
		<div class="filled"></div>
	</div>
	<div class="square-flip">
		<div class="filled"></div>
	</div>
	<div class="square-flip">
		<div class="spacer"></div>
		<div class="filled"></div>
	</div>
</div>
