<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<?php
if ($executed) {
	$status_class = "archived";
	$status_label = $text->get("patch_status_archived");
} else {
	$status_class = "new";
	$status_label = $text->get("patch_status_new");
}

$details = $text->get("patch_click_to_execute");
if ($executed) {
	$date = new DateTime($executed);
	$details = $text->get("patch_executed") . " " . $date->format("d.m.Y H:i");
}

?>

<li class="patch" data-patch="<?= $timestamp ?>">
	<div class="status <?= $status_class ?>">
		<span class="bold"><?= $status_label ?></span>
	</div>
	<div class="content">
		<div class="left">
			<div class="code">
				<span class="bold">Patch-<?= $timestamp ?></span>
			</div>
			<div class="name">
				<span><?= $name ?></span>
			</div>
		</div>
		<div class="right">
			<div class="details">
				<span><?= $details ?></span>
			</div>
		</div>
	</div>
	<div class="action">
		<a class="btn btn-loading <?= ($executed) ? "unarchive" : "archive" ?>" href="javascript:;">
			<?php if ($executed): ?>
			<span class="text"><?= file_get_contents(base_url() . "assets/icons/redo.svg") ?></span>
			<?php else: ?>
			<span class="text"><?= file_get_contents(base_url() . "assets/icons/archive.svg") ?></span>
			<?php endif; ?>
			<span class="loader"><?php $this->load->view("panda/layout/loader_square_flipper") ?></span>
		</a>
	</div>
</li>
