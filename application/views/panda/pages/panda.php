<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<?php if ($device === "mobile") { ?>
<script src="<?= base_url() ?>assets/js/mobile_menu.js" type="text/javascript"></script>
<header>
	<div class="panda">
		<a href="<?= base_url() . "panda/dashboard" ?>"><?= file_get_contents(base_url() . "assets/icons/panda.svg") ?></a>
	</div>
	<?php $this->load->view("layout/hamburger") ?>
	<?php $this->load->view("panda/layout/nav") ?>
</header>
<?php } else { ?>
	<?php $this->load->view("panda/layout/nav") ?>
<?php } ?>

<main>
	<div class="content-wrapper content-<?= isset($page_layout) ? $page_layout : "fixed" ?>">
		<div class="page-header">
			<h1 class="page-title"><?= $page_title ?></h1>
			<span class="page-subtitle"><?= $page_subtitle ?></span>
		</div>

		<?php $this->load->view("panda/pages/$page") ?>
	</div>
</main>
