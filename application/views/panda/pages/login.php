<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/panda/login.css">
<script src="<?= base_url() ?>assets/js/libraries/parsley.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/panda/login.js" type="text/javascript"></script>

<section id="login">
	<div class="content">
		<div id="panda">
			<img src="<?= base_url() ?>assets/icons/panda-midnight.svg" alt="Panda"/>
		</div>
		<form id="login-form" method="POST" data-parsley-validate>
			<div class="form-group">
				<label for="user"><?= $text->get("label_username") ?></label>
				<div class="icon-input">
					<span class="icon"><?= file_get_contents(base_url() . "assets/icons/user.svg") ?></span>
					<input id="user" class="full-width" type="text" name="username" placeholder="<?= $text->get("placeholder_username") ?>" required tabindex="1" autofocus
								 data-parsley-required-message="<?= $text->get("parsley_username_required") ?>"/>
				</div>
			</div>
			<div class="form-group">
				<label for="password"><?= $text->get("label_password") ?></label>
				<div class="icon-input">
					<span class="icon"><?= file_get_contents(base_url() . "assets/icons/lock.svg") ?></span>
					<input id="password" class="full-width" type="password" name="password" placeholder="<?= $text->get("placeholder_password") ?>" required tabindex="2"
								 data-parsley-required-message="<?= $text->get("parsley_password_required") ?>"/>
					<span class="icon right password-visibility"><?= file_get_contents(base_url() . "assets/icons/eye.svg") ?></span>
				</div>
			</div>
			<button class="btn btn-primary full-width" type="submit" name="login" value="login" tabindex="3">
				<span class="text"><?= $text->get("btn_login") ?></span>
				<span class="loader"><?php $this->load->view("panda/layout/loader_square_flipper") ?></span>
			</button>
			<div class="error-container">
				<span class="error invalid hidden"><?= $text->get("message_wrong_login") ?></span>
				<span class="error server-error hidden"><?= $text->get("message_login_error") ?></span>
			</div>
		</form>
	</div>
</section>
