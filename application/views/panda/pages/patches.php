<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/panda/progressbar.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/panda/patches.css">
<script src="<?= base_url() ?>assets/js/panda/progressbar.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/panda/search.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/panda/patches.js" type="text/javascript"></script>

<?php $this->load->view("panda/modals/info_modal") ?>

<section id="patches">
	<div id="patch-execution-progress" class="progress-bar"></div>
	<a id="execute-patches" href="javascript:;" class="btn btn-primary btn-loading <?= ($device === "mobile") ? "pull-left" : "pull-right" ?>" tabindex="1">
		<span class="text"><?= $text->get("btn_execute_patches") ?></span>
		<span class="loader"><?php $this->load->view("panda/layout/loader_square_flipper") ?></span>
	</a>
	<div class="clearfix"></div>
	<div id="new-patches">
		<ul>
		</ul>
	</div>
	<div id="archive">
		<div class="header">
			<h2><?= $text->get("heading_patches_archive") ?></h2>
			<div class="icon-input">
				<span class="icon"><?= file_get_contents(base_url() . "assets/icons/search.svg") ?></span>
				<input type="text" class="search" name="search" placeholder="<?= $text->get("placeholder_search_patches") ?>" tabindex="2"/>
				<span class="icon right date-picker"><?= file_get_contents(base_url() . "assets/icons/calendar.svg") ?></span>
			</div>
			<div class="search-result-list hidden">
				<ul class="search-results">
				</ul>
			</div>
		</div>
		<div class="clearfix"></div>
		<ul class="archive-list">
		</ul>
		<div class="load-more-results-container">
			<a href="javascript:;" class="btn btn-default btn-loading load-more-results" data-offset="0">
				<div class="scroll-controlled"></div>
				<span class="text"><?= $text->get("btn_load_more") ?></span>
				<span class="loader"><?php $this->load->view("panda/layout/loader_square_flipper") ?></span>
			</a>
		</div>
	</div>
</section>
