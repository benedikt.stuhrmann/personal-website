<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<?php $this->load->view("panda/modals/info_modal") ?>
<?php $this->load->view("panda/modals/confirm_modal") ?>

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/splide.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/panda/dashboard.css">
<script src="<?= base_url() ?>assets/js/libraries/splide.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/panda/dashboard.js" type="text/javascript"></script>

<script src="https://cdn.jsdelivr.net/npm/gridstack@2.2.0/dist/gridstack.all.js"></script>
<link href="https://cdn.jsdelivr.net/npm/gridstack@2.2.0/dist/gridstack.min.css" rel="stylesheet"/>

<script src="<?= base_url() ?>assets/js/libraries/chart.min.js" type="text/javascript"></script>

<script src="<?= base_url() ?>assets/js/libraries/openweathermap.js" type="text/javascript"></script>

<div id="performance-arrow-icon" class="hidden"><?= file_get_contents(base_url() . "assets/icons/performance-arrow.svg") ?></div>

<div id="weather-icon-sunny" class="hidden"><?= file_get_contents(base_url() . "assets/icons/weather-sunny.svg") ?></div>
<div id="weather-icon-clouds" class="hidden"><?= file_get_contents(base_url() . "assets/icons/weather-clouds.svg") ?></div>
<div id="weather-icon-light-drizzle" class="hidden"><?= file_get_contents(base_url() . "assets/icons/weather-light-drizzle.svg") ?></div>
<div id="weather-icon-drizzle" class="hidden"><?= file_get_contents(base_url() . "assets/icons/weather-drizzle.svg") ?></div>
<div id="weather-icon-light-rain" class="hidden"><?= file_get_contents(base_url() . "assets/icons/weather-light-rain.svg") ?></div>
<div id="weather-icon-rain" class="hidden"><?= file_get_contents(base_url() . "assets/icons/weather-rain.svg") ?></div>
<div id="weather-icon-thunder" class="hidden"><?= file_get_contents(base_url() . "assets/icons/weather-thunder.svg") ?></div>
<div id="weather-icon-light-snow" class="hidden"><?= file_get_contents(base_url() . "assets/icons/weather-light-snow.svg") ?></div>
<div id="weather-icon-snow" class="hidden"><?= file_get_contents(base_url() . "assets/icons/weather-snow.svg") ?></div>
<div id="weather-icon-fog" class="hidden"><?= file_get_contents(base_url() . "assets/icons/weather-fog.svg") ?></div>
<div id="weather-icon-hail" class="hidden"><?= file_get_contents(base_url() . "assets/icons/weather-hail.svg") ?></div>

<a href="javascript:;" class="btn btn-primary" data-action="edit-layout">Layout ändern</a>
<a href="javascript:;" class="btn btn-primary hidden" data-action="save-layout">Layout speichern</a>
<a href="javascript:;" class="btn btn-danger hidden" data-action="delete-widget">Hierher ziehen zum Löschen</a>

<section id="dashboard" class="grid-stack"></section>
