<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<!DOCTYPE html>
<html lang="<?= $text->getLanguage() ?> ">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= $title ?></title>

		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/panda/main.css">

		<script src="<?= base_url() ?>assets/js/libraries/jquery-3.3.1.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>assets/js/libraries/moment.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>assets/js/panda/main.js" type="text/javascript"></script>
	</head>
	<body>
		<script>
      const BASE_URL = "<?= base_url() ?>";
      const LANG = "<?= $text->getLanguage() ?>";
      const DEVICE = "<?= $device ?>";
		</script>
		<?php $this->load->view("panda/pages/$view") ?>
	</body>
</html>
